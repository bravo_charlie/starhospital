@extends('layouts.app')

@section('content')
 
<div class="modal fade" id="NoticeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <?php $count=1; ?>
    @foreach($modals as $m)
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLongTitle">{{$m->modalname}} </h4>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
          <p>
                    </p><center><img class="img-responsive" style="width:100%;height:auto;" src="{{ asset('/uploads/modal/'.$m->image )}}"></center>
                <p></p> 
                <p class="text-center">
                    </p><p style="text-align: justify;"><br></p>

                <p></p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-warning" data-dismiss="modal" style="background:red;color:#fff">Close</button>
      
      </div>
    </div>
    <?php $count ++;
    if($count > 1)
        break;
    ?>
      @endforeach
   </div>
</div>


<section class="slider-section">
  <div id="demo" class="carousel slide" data-ride="carousel">
<ol class="carousel-indicators">
   @foreach($sliders as $key => $slider)
    <li data-target="#demo" data-slide-to="{{$slider->id}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
 @endforeach
</ol>
    <!-- The slideshow -->
  <div class="carousel-inner">
     @foreach($sliders as $key => $slider)
    <div class="carousel-item {{ $key == 0 ? ' active' : '' }}">
      @if($slider->link)
      <a href="{{$slider->link}}" target="_blank"><img src="/uploads/slider/{{$slider->image}}"></a>
     @else
   <a href="#"><img src="/uploads/slider/{{$slider->image}}"></a>
      @endif
    </div>
     @endforeach
  </div>

  <!-- Left and right controls -->
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div>
</section>
<section class="newsreport">
  <div class="container">
    <h1 class="about__heading text-center" >
     Welcome to Star Hospital</h1>
     <h4 class="about__headingsub text-center" >
       Compassionate Care With Comfort
     </h4>
     <div class="row">
      <div class="col-md-3 col-sm-6 starcovid" style="text-align:center">
        <h6 >
        STAR HOSPITAL HOUSEPITAL SERVICE
        </h6>
       <br>
       <div class="button text_centered">
        <a href="/image/housepital-service.jpg" target="_blank" class="btn btn-danger" style="font-size:18px;">Housepital Service</a>
      </div>
    </div>
    <div class="col-md-3 col-sm-6 starcovid" style="text-align:center">
      <h6>
        STAR TELE-CONSULTATION SERVICE
      </h6>
      <br>
      <div class="button text_centered">
       <a href="https://patient.starhospitallimited.com/" target="_blank" class="btn btn-success" style="font-size:18px;">Tele-Consultation</a>
     </div>
   </div>
   <div class="col-md-3 col-sm-6 starcovid" style="text-align:center">
     <h6>
       STAR HOSPITAL ASSURRANCE  SERVICE
     </h6>
     <br>
     <div class="button text_centered">
      <a href="#" target="_blank" class="btn btn-primary" style="font-size:18px;">Star Assurrance</a>
    </div>
  </div>
  <div class="col-md-3 col-sm-6 starcovid" style="text-align:center">
   <h6>
    STAR LAB REPORTS/INVESTIGATION
   </h6>
   <br>
   <div class="button text_centered">
    <a href="https://patient.starhospitallimited.com/login" target="_blank" class="btn btn-warning" style="font-size:18px;color:white;">Investigation/Reports</a>
  </div>
</div>

</div>
</div>
</section>

<section class="about">
  <div class="container about__container">
    <p class="about__text">
      Star Hospital was established in 2007 as a 50-bedded, multispecialty hospital. On May 30, 
      2017, the hospital shifted to its own premises in Sanepa-2, Lalitpur. It is the first
      hospital to meet the standards set by the Ministry of Health and Population of Nepal.
      Our 100-bedded hospital has 22 specialty centres and features state-of-the-art modular
      operation theatres.
      Star Hospital also owns and manages two educational institutions: Modern
      Technical College and Star Academy. Modern Technical College offers Bachelor’s
      in Medical Laboratory Technology and produces proficient bachelor-level lab 
      technicians, and Star Academy offers PCL Nursing course.
      Our long-term vision is to establish a well-equipped multi-specialty hospital
      which would be able to give quality services at reasonable cost under the same
      umbrella with a motto of “Compassionate Care With Comfort”.</p>
      <div class="button text_centered margin_top_5">
        <a href="/About" class="button__btn button__btn--about">Learn More</a>
      </div>
    </div>
  </section>

  <section class="bg-grey pt-3">
    <div class="container text-center">
      <div class="inner-content-header forbest text-center col-md-10 offset-md-1">
        <h1>
          Our Health Professional Teams
        </h1>
        <br>
        <span>
          <p style="font-size:15px;text-align:center;">
            Star Hospital is home to some of the most eminent doctors in Nepal, most of whom are the 
            finest in their respective arenas and are renowned for developing innovative and revolutionary
            procedures.
          </p>
        </span>
         </div>
    </div>
    <div class="container my-4">
      <div id="multi-item-example" class="carousel slide carousel-multi-item" data-ride="carousel">
        <ol class="carousel-indicators">
          <li data-target="#multi-item-example" data-slide-to="0" class="active"></li>
          <li data-target="#multi-item-example" data-slide-to="1"></li>
          <li data-target="#multi-item-example" data-slide-to="2"></li>
        </ol>
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active">
            <div class="row">
              <div class="col-md-4">
                <div class="card mb-2">
                  <img class="card-img" src="/image/sunil_shrestha.jpg"
                  alt="Card image cap">
                  <div class="card-body">
                    <h5 class="card-title font-weight-bold">Dr. Sunil Shrestha</h5>
                  </div>
                </div>
              </div>
              <div class="col-md-4 clearfix d-none d-md-block">
                <div class="card mb-2">
                  <img class="card-img" src="/image/dr_shail-670x500.jpg"
                  alt="Card image cap">
                  <div class="card-body">
                    <h4 class="card-title font-weight-bold">Dr. Shail Rupakheti</h4>
                  </div>
                </div>
              </div>
              <div class="col-md-4 clearfix d-none d-md-block">
                <div class="card mb-2">
                  <img class="card-img" src="/image/anil_pokhrel.jpg"
                  alt="Card image cap">
                  <div class="card-body">
                    <h4 class="card-title font-weight-bold">Dr. Anil Pokhrel</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="carousel-item">
            <div class="row">
              <div class="col-md-4">
               <div class="card mb-2">
                <img class="card-img" src="/image/niranjan_palikhe.jpg"
                alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">Dr. Niranjan Palikhe</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 clearfix d-none d-md-block">
              <div class="card mb-2">
                <img class="card-img" src="/image/sumanbabu.jpg"
                alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">Dr. Suman Babu Marahatta</h4>
                </div>
              </div>
            </div>
            <div class="col-md-4 clearfix d-none d-md-block">
              <div class="card mb-2">
                <img class="card-img" src="/image/LAXMI-ACHARYA-GAUTAM-670x500.jpg"
                alt="Card image cap">
                <div class="card-body">
                  <h4 class="card-title font-weight-bold">Dr. Laxmi Acharya Gautam</h4>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="row">
            <div class="col-md-4">
             <div class="card mb-2">
              <img class="card-img" src="/image/satyam_rimal.jpg"
              alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title font-weight-bold">Dr. Satyam Rimal</h4>

              </div>
            </div>
          </div>
          <div class="col-md-4 clearfix d-none d-md-block">
            <div class="card mb-2">
              <img class="card-img" src="/image/dirgha_rc.jpg"
              alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title font-weight-bold">Dr. Dirgha Raj RC</h4>
              </div>
            </div>
          </div>
          <div class="col-md-4 clearfix d-none d-md-block">
            <div class="card mb-2">
              <img class="card-img" src="/image/niroj_hirachan.jpg"
              alt="Card image cap">
              <div class="card-body">
                <h4 class="card-title font-weight-bold">Dr. Niroj Hirachan</h4>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div> 
<div class="container text-center"> 
        <a href="/Doctor" class="btn btn-custom readmore">
          View All
        </a>  
</div>
</section>
<section class="bg-grey features-var-two clearfix">
  <div class="container">
    <div class="row">
      <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
        <div class="row">
          <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
            <img src="/image/health-care.png" alt="24 Hours Health Services">                            </div>
            <div class="col-lg-9 col-md-9 col-sm-9">
              <h3>
               24 Hours Health Services                            </h3>
               <p class="passageintro">Various 24 Hours Health Services : <br>
                Emergency Service, Pathology Service, Ambulance Service, Pharmacy Service,  Radiology Service, Trauma and Orthopedic Service
              </p>
            </div>
          </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
          <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
              <img src="/image/nepal-goverment-logo.jpg" alt=" State-of-art infrastructure">                            </div>
              <div class="col-lg-9 col-md-9 col-sm-9">
                <h3>
                  State-of-art infrastructure                            </h3>
                  <p class="passageintro">One of the first hospital to meet the standards set by the Ministry of Health and Population of Nepal</p>
                </div>
              </div>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
              <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
                  <img src="/image/icon-1.png" alt="Speciality Centers">                            </div>
                  <div class="col-lg-9 col-md-9 col-sm-9">
                    <h3>
                     Speciality Centers                            </h3>
                     <p class="passageintro">Our 100-bedded hospital has 22 specialty centers.</p>
                   </div>
                 </div>
               </div>
               <div class="visible-lg clearfix"></div>
               <div class="visible-md clearfix"></div>
               <div class="visible-sm clearfix"></div>
               <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
                <div class="row">
                  <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
                    <img src="/image/earthquake.png" alt="Earthquake-resistant building">                            </div>
                    <div class="col-lg-9 col-md-9 col-sm-9">
                      <h3>
                       Earthquake-resistant building                            </h3>
                       <p class="passageintro">The infrastructure of Star Hospital is earthquake resistant.</p>
                     </div>
                   </div>
                 </div>
                 <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
                  <div class="row">
                    <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
                      <img src="/image/icon-2.png" alt="Operation Theatres">                            </div>
                      <div class="col-lg-9 col-md-9 col-sm-9">
                        <h3>
                         Operation Theatres                            </h3>
                         <p class="passageintro">State-of-the-art modular operation theatres</p>
                       </div>
                     </div>
                   </div>
                   <div class="col-lg-4 col-md-4 col-sm-4 single-feature">
                    <div class="row">
                      <div class="col-lg-3 col-md-3 col-sm-3 icon-wrapper">
                        <img src="/image/1st-big-feature.png" alt="Easy Access">                            </div>
                        <div class="col-lg-9 col-md-9 col-sm-9">
                          <h3>
                           Easy Access                            </h3>
                           <p class="passageintro">Located near Sanepa-Ring Road, which makes it easily accessible</p>
                         </div>
                       </div>
                     </div>
                     <div class="visible-lg clearfix"></div>
                     <div class="visible-md clearfix"></div>
                     <div class="visible-sm clearfix"></div>
                   </div>
                 </div>
               </section>

               <section class="media-center section-margin ">
                <div class="container">
                  <div class="inner-content-header text-center">
                    <h1>
                      News/Events
                    </h1>
                  </div>

                  <div class="row">
                    <div class="col-lg-4 col-md-6 newspanel">
                      <div class="media-content">
                        <div id="carouselExampleIndicators1" class="carousel slide carouselSlide" data-ride="carousel">
                         <ol class="carousel-indicators">
                          @foreach($allnews as $key => $news)
                          <li data-target="#carouselExampleIndicators1" data-slide-to="{{$news->id}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
                          @endforeach
                        </ol>
                        <div class="carousel-inner">
                          @foreach($allnews as $key => $news)
                          <div class="carousel-item {{ $key == 0 ? ' active' : '' }}">
                           <img class="imageheightnew" style="max-height:190px;min-height:190px" src="uploads/news/{{$news->image}}" alt="First slide">
                           <a href="/newsdetails/{{$news->id}}">
                             <p class="txt-blue">
                              {{$news->newstitle}}
                            </p>
                          </a>
                        </div>
                        @endforeach
                      </div>
                    </div>

                    <a href="/News">
                     <div class="media-title-holder">
                      <h5>
                        View all News
                      </h5>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 newspanel">
                <div class="media-content">
                  <a href="/gallery">
                    <img src="/image/gallery-min.png" alt="medicity-video-image1">
                    <div class="media-title-holder">
                      <h5>
                        Photo Gallery
                      </h5>
                    </div>
                  </a>
                </div>
              </div>
              <div class="col-lg-4 col-md-6 newspanel">
                <div class="media-content">
                  <div id="carouselExampleIndicators1" class="carousel slide carouselSlide" data-ride="carousel">
                   <ol class="carousel-indicators">
                    @foreach($allevents as $key => $events)
                    <li data-target="#carouselExampleIndicators1" data-slide-to="{{$events->id}}" class="{{ $key == 0 ? ' active' : '' }}"></li>
                    @endforeach
                  </ol>
                  <div class="carousel-inner">
                    @foreach($allevents as $key => $events)
                    <div class="carousel-item {{ $key == 0 ? ' active' : '' }}">
                     <img class="imageheight" style="max-height:205px;min-height:205px" src="uploads/events/{{$events->image}}" alt="First slide">
                     <a href="/eventdetail/{{$events->id}}">
                       <p class="txt-blue">
                        {{$events->event_name}}
                      </p>
                    </a>
                  </div>
                  @endforeach
                </div>
              </div>
              <a href="/eventdetails">
                <div class="media-title-holder">
                  <h5>
                   View all Events
                 </h5>
               </div>
             </a>
           </div>
         </div>


       </div>
     </div>
   </section>
   <section class="wrapper equipment">
    <section class="container pltr">
      <h2>Centres of Excellence</h2>
      <h4>Combining the best specialists and equipment to provide you nothing short of the best in healthcare</h4>
      <section class="equipment-slider">
       <div id="flexslider-93" class="flexslider  theme-without-shadow theme-border-00 theme-border-radius-00  
       default  bg-caption-black-02 position-caption-bottom-centered color-nav-dark color-nav-active-black">
      <div class="flex-viewport" style="overflow: hidden; position: relative;">
        
        <ul class="slides" style="width: 2600%; transition-duration: 1.5s; transform: translate3d(-760px, 0px, 0px);">
          <li style="width: 190px; float: left; display: block;">
            <a target='_self' href='#'>
              <figure>
                <img title='Cardiology' alt='Cardiology' src="/image/heart.png" class='img-responsive' /></figure>
                <aside class='desc'>
                  <h3 class='flex-caption'>Cardiology</h3>
                </aside>
              </a>
            </li>
            <li style="width: 190px; float: left; display: block;">
              <a target='_self' href='#'>
                <figure>
                  <img title='Spine Surgery' alt='Spine Surgery' src="/image/spine.png" class='img-responsive' /></figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Spine Surgery</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Orthopedic Hospital In India' alt='Orthopedic Hospital' src="/image/orthopaedics.png" class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Orthopedic Hospital</h3>
                  </aside>
                </a>
              </li>
             
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Diabetes' alt='Diabetes' src='/image/diabetic.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Diabetes</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Neurology And Neurosurgery' alt='Neurology And Neurosurgery' src='/image/neurosciences.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Neurology And Neurosurgery</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Best Gastroenterology Hospital' alt='Best Gastroenterology Hospital In India' src='/image/gastroenterology.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Best Gastroenterology Hospital</h3>
                  </aside>
                </a>
              </li>
              
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Best Critical Care Hospital' alt='Best Critical Care Hospital In India' src='/image/critical-care.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Best Critical Care Hospital</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Bariatric Surgery' alt='Bariatric Surgery' src='/image/bariatric-surgery.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Bariatric Surgery</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Preventive Medicine' alt='Preventive Medicine' src='/image/preventive-medicine.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Preventive Medicine</h3>
                  </aside>
                </a>
              </li>
              <li style="width: 190px; float: left; display: block;">
                <a target='_self' href='#'>
                  <figure>
                    <img title='Emergency' alt='Emergency' src='/image/emergency.png' class='img-responsive' />
                  </figure>
                  <aside class='desc'>
                    <h3 class='flex-caption'>Emergency</h3>
                  </aside>
                </a>
              </li> 
            </ul>
          </div>
          <ul class="flex-direction-nav">
            <li class="flex-nav-prev">
              <a class="flex-prev" href="#">Previous</a>
            </li>
            <li class="flex-nav-next">
              <a class="flex-next" href="#">Next</a>
            </li>
          </ul>
      
 
        </div>
      </section>
    </section>
  </section>
  <section class="bg-blue section-margin">
    <div class="container">
      <h3 class="goodhandtext">
       Our Patients. Their Stories.
     </h3>
     <div class="row" style="text-align:center;">
      <div class="col-md-6">
       <div class="testimonial-Slider">
        <div  class="carousel slide" data-ride="carousel">
          <div class="carousel-inner" role="listbox">
           @foreach($testimonials as $key => $testimonial)
           <div class="carousel-item{{ $key == 0 ? ' active' : '' }}">                   

            <div class="testimonial-caption carousel-caption">
              <p class="patientspeak">
               <?php echo ($testimonial->description ) ?>

             </p>
             <h5 style="float:left;"><strong>{{$testimonial->name}}</strong></h5>

           </div>
         </div>
         @endforeach   
       </div>
     </div>
   </div>
 </div>
 <div class="col-sm-6">
  <img title='Preventive Medicine' alt='Preventive Medicine' src='/image/testimonial_img.png' class='img-responsive' />
</div>
<a href="/testimonial" class="btn btn-custom testimonial">
  View More Testimonial
</a> 
</div>

</div>
</section>


@endsection
