@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Feedback
		</h5>
	</div>
</section>
 <section class="contact-form">
                    <div class="container">
                        <div class="col-md-12 mt-3 bg-white  shadow-box  md-3">
                   
                            <div class="row">
                                <div class="col-md-12 bg-white pd-3">
                                    <form action="/sendemail/feedback" method="post" id="FormCreate">
                                        @csrf
                                       <div class="row mt-3">
                                           <div class="col-md-12">
                                            <h5>
                                                Feedback / Complaints / Suggestion Form
                                                <span class="pull-right">
                                                    <i class="fa fa-paper-plane"></i>
                                                </span>
                                            </h5>
                                            <hr>
                                          </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Full Name <span class="required">*</span></label>
                                            <input Placeholder="Full Name" class="form-control" id="FullName" name="FullName" required="required" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input Placeholder="example@email.com" class="form-control" id="Email" name="Email" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Mobile <span class="required">*</span></label>
                                            <input Placeholder="Phone" class="form-control" id="Phone" name="Phone" required="required" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Select Packages <span class="required">*</span></label>
                                            <select class="form-control" id="MessageType" name="MessageType" required="required"><option value="">--- Select Message Type ---</option>
                                                <option value="Feedback">Feedback</option>
                                                <option value="Complaint">Complaint</option>
                                                <option value="Suggestion">Suggestion</option>
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <br>
                                            <label for="exampleInputEmail1">Message <span class="required">*</span></label>
                                            <textarea Placeholder="Message" class="form-control" id="Message" name="Message" required="required" rows="10">
                                            </textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-danger btn-100" data-dismiss="modal" style="width:84px;">Close</button>
                                            <button type="submit" class="btn btn-custom btn-100" style="width:84px;">
                                                Send
                                            </button>
                                        </div>
                                    </div>
                                 </form>                                
                               </div>
                            </div>
                        </div>
                    </div>
                </section>

@endsection