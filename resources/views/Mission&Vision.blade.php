@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
   		<h5>
			Mission and Vision
		</h5>
    </div>
</section>
<section class="content-style-1 section-margin ">
    <div class="container">
        <div class="row">
        	<div class="col-md-12">
        		<div class="inner-content-header">
                    <h1>
                       <b> Mission</b>
                    </h1>
                </div>
                <p>
                	Star hospital is a multi-specialty state of art Hospital committed to provide 
                	healthcare services to our patients at an reasonable cost. It also incorporates 
                	the different academic programs for producing healthcare personnel needed for the 
                	Health sector of country. Our main motto is to give “Compassionate 
                	Care with Comfort”.
                </p>
                
            </div>
            <div class="col-md-12">
                <div class="inner-content-header">
                    <h1>
                      <b> Vision</b> 
                    </h1>
                </div>
                <p>
                Our long term vision is to establish well equipped superficiality hospital with all the
                 services under the same roof so that people from every walk of life can get these 
                 services at an reasonable cost. Our vision is also to train allied health professionals 
                 needed for the country.
		        </p>
             </div>
       </div>
    </div>
</section>


@endsection