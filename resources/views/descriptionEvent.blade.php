@extends('layouts.app')

@section('content')
<section class="parallax">
    <div class="container parallax-content">
        <h5>
            Events 
        </h5>
    </div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100">
         <div id="eventContent" class="col-md-9 p-4 bg-white"></div>
         <div id="eventContentNormal" class="col-md-9 p-4 bg-white">
                <h4 class="txt-blue">
                   {{$events->event_name}}
                </h4><br>
                  <img src="/uploads/events/{{$events->image}}" class="img-responsive" alt="">
                <br><br>
                <?php echo ($events->event_description ) ?>
                <br>
            </div>

            <div class="col-3 collapse d-md-flex pt-2 h-100" id="sidebar">
                <ul class="nav sidebar-nav">
                   @foreach($allevents as $events)
                    <li class="nav-item news <?php if($events->id==$id)echo 'active';?>">
                        <a class="nav-link" id="lnk{{ $events->id }}" onclick="callAjax(event, {{ $events->id }})">{{$events->event_name}}</a>
                    </li>
                   @endforeach
                </ul>
            </div>
         </div>
         <script type="text/javascript">

          function callAjax(evt, id){
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           $.ajax({
              type: 'GET',
              url: '/eventdetailsAjax/',
              data: {'id' : id},
              success: function (response) {
                  
                  $('#eventContent').show();
                  $( "#eventContentNormal" ).hide();
                  $('#eventContent').html(response);
                
              }
            });
        }
     </script>
    </div>
 </section>
@endsection