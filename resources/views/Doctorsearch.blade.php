<section class="bg-grey" id="doctorsearch">
	<div class="container pt-3 pb-3 h-100">
		<div class="row h-100">

			@if(count($doc))
			@foreach($doc as $doctor)
			<div class="col-lg-4 col-md-6 doctorssearch" style="margin-bottom:20px;">
			    
						<div class="card-desk bg-white doctordetails search">
							<a href="/doctordetails/{{$doctor->id}}">
								<img src="uploads/doctor/{{$doctor->image}}" alt="image">
							</a>
						       <div class="card-header-content">
        								<a href="/doctordetails/{{$doctor->id}}">
        									<h2>
        										<b>{{$doctor->name}} </b>
        									</h2>
        								</a>

        								<h6>
        									@foreach($departments as $department)
        									@if($department->id == $doctor->dept_id)
        									{{$department->departmentname}}
        									@endif
        									@endforeach
        								</h6>
        								<hr>
        					       </div>
        					       <ul class="ul-unstyled list-btn">
        								<li>
        									<a href="/doctordetails/{{$doctor->id}}" role="button" class="btn btn-sm btn-lined btn-sm">
        										View Profile
        									</a>

        								</li>

        								<li>
        									<a href="#" role="button" class="btn btn-sm  btn-custom">
        										Book Appointment
        									</a>
        								</li>
        							</ul>
					 	 </div>
                     </div>
			@endforeach
			@else
			<h4>Search Result do not match any records.</h4>
			@endif
		</div>
	</div>
</section>



