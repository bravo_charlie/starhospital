@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
   
		<h5>
			Message From Medical Director
		</h5>

</div>
</section>
<section class="content-style-1 section-margin ">
    <div class="container">
        <div class="row">
        	<div class="col-md-6">
                <img src="/image/Shail-Rupakheti-670x500 (1).jpg" class="img-responsive" alt="star-hospital" style="box-shadow: 1px 1px 5px #379e50; margin-top: 16px;">
            </div>
            <div class="col-md-6">
                <div class="inner-content-header">
                    <h1>
                        
                    </h1>
                    
                </div>
                <p>
                 Greetings from Star Hospital.<br>
                
                 After 11 years of providing healthcare services to Nepalese of all ages, sexes, races 
                 and socioeconomic statuses, we at Star Hospital endeavoured to move to a new, 
                 better-equipped and centrally located building owned by Star Hospital itself. 
                 And owing to the support of our well-wishers, patients and promoters, we have
                  successfully been able to achieve that feat. Our new hospital sprawls over an 
                  area of 80 thousand square feet, and is situated at a highly convenient and accessible 
                  location next to the Ring Road at Sanepa Height-2. With modern-day 
                  multispecialty-hospital facilities and a competent team of doctors and healthcare 
                  providers, Star Hospital is more than ready to provide to our patients “Compassionate 
                  Care with Comfort”, which is our new slogan.<br>

                 Star Hospital prides itself in having one of the cleanest and most comfortable 
                 facilities, managed by staff who are trained to provide services with a smile. 
                 The hospital houses 100 beds, and has spacious and clean rooms. Our emergency rooms 
                 operate round the clock, all year round. Our reception and lobby on the ground floor
                  are manned by trained staff who will ensure your experience at Star Hospital is no 
                  less than that in a star-rated hotel. Furthermore, our highly organised and 
                  well-stocked pharmacy ensures that there will be no hassle whatsoever while
                   purchasing prescribed pharmaceuticals.<br>

                  Star Hospital features a separate outpatient department with its own reception and 
                  billing counter, a wellness center, physiotherapy facilities, a dental department 
                  and a separate X-ray department. Moreover, most of the diagnostic services—including
                   upper and lower gastrointestinal endoscopy, electrocardiography, 
                   electroencephalography, pulmonary function tests, and blood, urine and stool 
                   collection—can be availed of on the same floor, so that both inpatients and 
                   outpatients at Star Hospital have a hassle-free experience.<br>

                   The hospital has 3 completely modular operation-theaters on the third floor, and an 
                   additional emergency operation theater has been set up close to the emergency rooms. 
                   The operation-theater floor is equipped with a fully monitored post-operative ward, 
                   an intensive care unit, a neonatal intensive care unit and a waiting area—which 
                   features a small cafe, counseling rooms and a prayer room—for visitors.<br>

                   The hospital is equipped with the newly launched 32-Slice CT Scanner, the latest 
                   ultrasound and echocardiography equipments and a central lab where all sorts of 
                   tests are performed. The hospital also has one super deluxe cabin and one semi-super
                   eluxe cabin with their own pantries, balconies and visitor rooms.<br>

                   This website is designed to provide you with a comprehensive tour of our hospital and
                   I would like to take this opportunity to invite you all to utilise our facilities. 
                   Please feel free to reach out to our team of competent and friendly doctors, 
                   healthcare providers and housekeeping staff, all of whom will try their best to 
                   ensure that you receive “Compassionate Care with Comfort” at Star Hospital.<br>

               <strong> DR. SHAIL RUPAKHETI  </strong>
	
		        </p>
             </div>
       </div>
    </div>
</section>

@endsection