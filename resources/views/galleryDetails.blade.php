@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Gallery Details
		</h5>
	</div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100">
            <h2>{{$galleries->name}}</h2>
            <div class="col-md-12 p-2 bg-white shadow-box md-2">
                <div class="row">
                @foreach($gallery as $galldetails)
                        <?php
                            $json = ($galldetails->image);
                             $arr = json_decode($json, true);
                             foreach($arr as $key=>$value){
                                 ?>
                    <div class="col-sm-4 detailsimage">    
                            <img src="/uploads/gallerydetails/{{$value}}">
                    </div>
                            <?php
                             }
                            echo "<hr>";
                            ?>
                @endforeach
            </div>
            </div>
        </div>
    </div>
</section>

@endsection