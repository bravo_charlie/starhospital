@extends('layouts.app')

@section('content')
<section class="parallax">
    <div class="container parallax-content">
       
        <!--<h5>
            News sunilmaharjan
        </h5> -->
    </div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100">
         <div id="newsContent" class="col-md-9 p-4 bg-white"></div>
         <div id="newsContentNormal" class="col-md-9 p-4 bg-white">
                <h4 class="txt-blue">
                   {{$news->newstitle}}
                </h4><br>
                  <img src="/uploads/news/{{$news->image}}" class="img-responsive" alt="">
                <br><br>
                <?php echo ($news->description ) ?>
                <br>
          </div>
            <div class="col-3 collapse d-md-flex pt-2 h-100" id="sidebar">
                <ul class="nav sidebar-nav">
                     @foreach($allnews as $news)
                    <li class="nav-item news <?php if($news->id==$id)echo 'active';?>">
                        <a class="nav-link" id="lnk{{ $news->id }}" onclick="callAjax(event, {{ $news->id }})">{{$news->newstitle}}</a>
                    </li>
                   @endforeach
                </ul>
            </div>
         </div>
         <script type="text/javascript">

          function callAjax(evt, id){
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           $.ajax({
              type: 'GET',
              url: '/newsdetailsAjax/',
              data: {'id' : id},
              success: function (response) {
                  
                  $('#newsContent').show();
                  $( "#newsContentNormal" ).hide();
                  $('#newsContent').html(response);
                
              }
            });
        }
    </script>
        </div>
 </section>
@endsection