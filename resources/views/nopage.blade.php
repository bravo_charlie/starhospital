
<!DOCTYPE html>
<head>
	
	<title>404 - Page</title>

<link rel="stylesheet" href="{{asset('css/34e94cf2db0090d93d3026b2af01e455.css')}}" />
	
</head>
<body id="error404" class="en">
	<div id="topWrapper">
		<header id="header" role="banner">
			<div id="titleWrapper">
				<div class="container">
				
					<div id="title">
						<h1>404</h1>					
					</div>
				</div>
			</div>
		</header>

		<div id="main" role="main">
			<div style="text-align: center;" class="container">
				<a href="/" class="btn btn-primary"><button>Go Back To MainPage</button></a>
			</div>
		</div>
	</div>

</body>
</html>