@extends('layouts.app')
<style type="text/css">
#magazine{
	width:800px;
	height:400px;
}
#magazine .turn-page{
	background-color:#ccc;
	background-size:100% 100%;
}
.brochure{
	padding:50px 0%;
}
</style>

@section('content')
<section class="brochure">
	<div class="container">
<div id="magazine">
	<div style="background-image:url(image/1.jpg);"></div>
	<div style="background-image:url(image/2.jpg);"></div>
	<div style="background-image:url(image/3.jpg);"></div>
	<div style="background-image:url(image/4.jpg);"></div>
	<div style="background-image:url(image/5.jpg);"></div>
	<div style="background-image:url(image/6.jpg);"></div>
	<div style="background-image:url(image/7.jpg);"></div>
	<div style="background-image:url(image/8.jpg);"></div>
</div>
</div>
</section>

<script type="text/javascript">

	$(window).ready(function() {
		$('#magazine').turn({
							display: 'double',
							acceleration: true,
							gradients: !$.isTouch,
							elevation:50,
							when: {
								turned: function(e, page) {
									/*console.log('Current view: ', $(this).turn('view'));*/
								}
							}
						});
	});
	
	
	$(window).bind('keydown', function(e){
		
		if (e.keyCode==37)
			$('#magazine').turn('previous');
		else if (e.keyCode==39)
			$('#magazine').turn('next');
			
	});

</script>


@endsection