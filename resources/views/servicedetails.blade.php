@extends('layouts.app')

@section('content')
<section class="parallax">
    <div class="container parallax-content">
        <h5>
            Our Services
        </h5>
    </div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
<div class="row h-100" >
 <div id="serviceContent" class="col-md-9 p-4 bg-white"></div>
 <div id="serviceContentNormal" class="col-md-9 p-4 bg-white">
     <h4 class="txt-blue">
        {{$services->servicename}}
     </h4><br>
       <img src="/uploads/service/{{$services->serviceimage}}" class="img-responsive" alt="">
     <br><br>
     <h5 class="txt-blue">
         Star Hospital  Services
     </h5>
     <?php echo ($services->servicedescription ) ?>
     <br>
    
 </div>
     <div class="col-3 collapse d-md-flex pt-2 h-100" id="sidebar">
               <ul class="nav sidebar-nav">
                     @foreach($allService as $service)
                    <li class="nav-item <?php if($service->id==$id)echo 'active';?>">
                        <a class="nav-link" id="lnk{{ $service->id }}" onclick="callAjax(event, {{ $service->id }})">{{$service->servicename}}</a>
                    </li>
                 @endforeach
                </ul>
      </div>
</div>
    <script type="text/javascript">
        function callAjax(evt, id){
            $.ajaxSetup({
                headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           $.ajax({
              type: 'GET',
              url: '/servicedetailsAjax/',
              data: {'id' : id},
              success: function (response) {
               
                  $('#serviceContent').show();
                  $( "#serviceContentNormal" ).hide();
                  $('#serviceContent').html(response);
              
              }
            });

       }
    </script>
  </div>
</section>
@endsection