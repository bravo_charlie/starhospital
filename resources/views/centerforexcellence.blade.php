@extends('layouts.app')

@section('content')

      
<section class="parallax">
    <div class="container parallax-content">
        <h5>
            Center Of Excellence
        </h5>
    </div>
</section>

<section class="bg-grey">

    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100 grid-ceo-listing">
           
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#" class="box-lisitng">
                                <img src="/image/neurophstiry.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Neuro-psychiatry
                                </h5>
                            </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                       <a href="#" class="box-lisitng">
                                <img src="/image/neurophstiry.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Pulmonology
                                </h5>
                            </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#" class="box-lisitng">
                               <span> <img src="/image/icon/diet.png" alt="medicity-image-3"></span>
                                <h5>
                                    Department of Clinical Nutrition and Dietetics 
                                </h5>
                            </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                           <img src="/image/generalsurgery.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of General Surgery
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/laboratory.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Laboratory Medicine &amp; Pathology
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/neuroscience.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Neurosciences
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                             <img src="/image/denstitry.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Dentistry
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                           <img src="/image/cardic.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Cardiac Sciences
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/digestiveimage.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Digestive Diseases
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/orthopedics.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Orthopedics &amp; Joint Reconstruction
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/motherchild.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Mother &amp; Child Care
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                             <img src="/image/cancer.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Cancer Care
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/medicine.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Diagnostic Medicine 
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/renalscience.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Renal Sciences
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/ent.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for ENT-Head &amp; Neck Surgery 
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/endocronology.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Internal Medicine, Endocrinology &amp; Wellness
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/perioperative.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Peri Operative Medicine
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/emergencymedicine.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Emergency Medicine &amp; Pre Hospital Care
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/physiotherapy.jpg" alt="medicity-image-3">
                                <h5>
                                    Center for Physiotherapy &amp; Rehabilitation
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/dermatology.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Dermatology
                                </h5>
                        </a>
                    </div>
                    <div class="col-lg-3 col-md-4 text-center">
                        <a href="#"  class="box-lisitng">
                            <img src="/image/othmology.jpg" alt="medicity-image-3">
                                <h5>
                                    Department of Ophthalmology 
                                </h5>
                        </a>
                    </div>
                 </div>
              </div>
       </section>

@endsection