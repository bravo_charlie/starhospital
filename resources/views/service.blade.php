@extends('layouts.app')

@section('content')


    
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Our Services
		</h5>
	</div>
</section>

<section class="bg-grey">
	<div class="container pt-3 pb-3 h-100">
		<div class="row h-100">
			<div class="col-3 collapse d-md-flex pt-2 h-100" id="sidebar">
				<ul class="nav sidebar-nav">
					<li class="nav-item"><a class="nav-link" href="#">Care Credit</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Abhibhawak Prati Samarpit</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Neighborhood</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Health At Home</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Ambulance Services</a></li>
					<li class="nav-item"><a class="nav-link" href="#">HelloHeli</a></li>
					<li class="nav-item"><a class="nav-link" href="#">Prenatal and Postnatal Classes</a></li>
				</ul>
			</div>
			<div class="col-md-6 p-2 bg-white">
				<h4 class="txt-blue">
					Care Credit
				</h4>
				
                        <img src="/image/Neighborhood.jpg" class="img-responsive" alt="">
				<br>
				<h5 class="txt-blue">
					Star Hospital  Services
				</h5>
				<p>
					<p style="font-size:16px;">
						<span style="font-size:16px;">Star Hospital Care Credit caters to the 
							financial need of the customers by providing easy financial scheme on 
							health care requirement. It is hassle-free and trustworthy credit facility
							llowing customers to pay for their medical requirements via Credit Card. 
							Nepalese with regular sources of income such as salary, rental and or
							 business income are eligible to obtain Care Credit facility. Customer 
							 can make easy repayment via Credit Card. To inquire and apply for this 
							 facility, customers can visit our dedicated counter at Star Hospital
							  or nearest MBL Branches as per convenience.</span></p>
						<p style="font-size:16px;">
							<span style="font-size:16px;">
								<span class="lead"><b>Features</b></span>
							</span>
						</p>
							<ul>
								<li>
								 <span style="font-size:16px;">Maximum loan amount NPR 700,000.00</span>
								</li>
								<li>
									<span style="font-size:16px;">Loan Tenure: Maximum 24 months</span>
								</li>
								<li>
									<span style="font-size:16px;">Repayment Method: EMI</span>
								</li>
							</ul>
						<p style="font-size:16px;">
						 <span style="font-size:16px;">
						 	<span class="lead"><b>Process</b></span>
						 </span>
						</p>
						<ul>
		  				 <li>
		            		<span style="font-size:16px;">Submit a duly filled form at Credit Care 
		            			Helpdesk
		            		 at Star Hospital along with Citizenship, Passport size 
		            		 Photograph along with income statement for last 3 months.</span>
		            		</li>
						<li>
							<span style="font-size:16px;">Have the loan approved within 2 working days.</span></li>
						</ul>
						<div id="selenium-highlight">
							&nbsp;</div>
							<div id="selenium-highlight">
								&nbsp;</div>

							</p>
						
	                 </div>
				    <div class="col-md-3 bg-white pd-3">
						<form action="/Services/care-credit" id="FormCreate" method="post">
						<input data-val="true" data-val-number="The field ServiceMainId must be a number." data-val-required="The ServiceMainId field is required." id="ServicesMain_ServiceMainId" name="ServicesMain.ServiceMainId" type="hidden" value="4" />                                   
						 <div class="col-md-12">
						 <h4 class="txt-blue">
							 Online Query
						 </h4>
					    </div>
						<div class="col-md-12">
						<label for="exampleInputEmail1">Name</label>
						<input class="form-control" data-val="true" data-val-required="Name Required" id="ServiceContactUsForm_FullName" name="ServiceContactUsForm.FullName" placeholder="Name" required="required" type="text" value="" />
						<span class="field-validation-valid" data-valmsg-for="ServiceContactUsForm.FullName" data-valmsg-replace="true"></span>
						</div>
	                    <div class="col-md-12">
						<label for="exampleInputEmail1">Address</label>
						<input class="form-control" data-val="true" data-val-required="Address Required" id="ServiceContactUsForm_Address" name="ServiceContactUsForm.Address" placeholder="Address" required="required" type="text" value="" />
						<span class="field-validation-valid" data-valmsg-for="ServiceContactUsForm.Address" data-valmsg-replace="true"></span>
						</div>
						<div class="col-md-12">
						<label for="exampleInputEmail1">Phone No</label>
						<input class="form-control" data-val="true" data-val-required="Phone No Required" id="ServiceContactUsForm_ContactNumber" name="ServiceContactUsForm.ContactNumber" placeholder="Phone No" required="required" type="text" value="" />
						<span class="field-validation-valid" data-valmsg-for="ServiceContactUsForm.ContactNumber" data-valmsg-replace="true"></span>
						</div>
						<div class="col-md-12">
						<label for="exampleInputEmail1">Email Id</label>
						<input class="form-control" data-val="true" data-val-required="Email Id Required" id="ServiceContactUsForm_EmailId" name="ServiceContactUsForm.EmailId" placeholder="Email Id" required="required" type="text" value="" />
						<span class="field-validation-valid" data-valmsg-for="ServiceContactUsForm.EmailId" data-valmsg-replace="true"></span>
						</div>
						<div class="col-md-12">
						<label for="exampleInputEmail1">Query</label>
						<textarea Placeholder="Query" class="form-control" cols="20" data-val="true" data-val-required="Query Required" id="ServiceContactUsForm_Query" name="ServiceContactUsForm.Query" required="required" rows="8">
						</textarea>
						<span class="field-validation-valid" data-valmsg-for="ServiceContactUsForm.Query" data-valmsg-replace="true"></span>
						</div>
						<div class="col-md-4 ">
						<label for="exampleInputEmail1"> </label>
						<button type="submit" class="btn btn-custom btn-100">
						Submit
						</button>

						</div>
						</form> 
						</div>
						</div>
					</div>
			</section>
@endsection