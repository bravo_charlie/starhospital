@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
		<h5>
			International Patient Center
		</h5>
	</div>
</section>

<section class="patient-documentary-section">
	<div class="container">
		<div class="col-md-12 mt-3 bg-white  shadow-box md-2">
			<br>
			<p>
				<strong>Get in touch with our concierge at the international desk, so that your trip can be planned for you as per your requirements.</strong></p>
				<p>
					&nbsp;</p>
					<p>
					<strong>Contact for more details:</strong></p>
					<p>
					Name: Dr. Sanjay Bhattachan</p>
					<p>
					Phone: +977-1-5450297</p>
					<p>
					Email: itdept@starhospitallimited.com</p>
					<br>
		</div>
		<div class="col-md-12 mt-3 bg-white  shadow-box appoinmentBox1 md-3">
			@if ($errors->any())
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif

					@if (Session::has('success'))
					<div class="alert alert-success text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<p>{{ Session::get('success') }}</p>
					</div>
					@endif
			<div class="row">
			  <div class="col-md-8 bg-white pd-3">
                    <form method="post" action="/sendemail/internationalparty" id="FormCreate" >
                       {!! csrf_field() !!}
                       	<div class="row mt-3">
                            <div class="col-md-12">
                                <h5>
                                    Your Details
                                </h5>
                                <hr>
                            </div>
                            <div class="form-group col-md-6">
									<input id="form_name" type="text" name="fullname" class="form-control" placeholder="Your Full Name *" required="required" data-error="Name is required.">
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group col-md-6">
									<input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group col-md-6">
									<input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile *" required="required" data-error="PhoneNumber is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="patient_age" type="text" name="age" class="form-control" placeholder="Your Age *" required="required" data-error="Age is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="nationality" type="text" name="nation" class="form-control" placeholder="Your Nationality *" required="required" data-error="Nationality is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="passportnumber" type="text" name="passportnumber" class="form-control" placeholder="Your Passport Number*" required="required" data-error="Passport Number is required.">
									<div class="help-block with-errors"></div>
								</div>

                                <div class="col-md-6">
                                    <label for="exampleInputEmail1">Gender <span class="required">*</span></label>
                                    <label class="check-style">
                                        Male
                                        <input checked="checked" id="Gender" name="Gender" type="radio" value="Male" />
                                     <span class="checkmark"></span>
                                    </label>
                                    <label class="check-style">
                                        Female
                                        <input id="Gender" name="Gender" type="radio" value="Female" />
                                        <span class="checkmark"></span>
                                    </label>
                                </div>
                                <div class="col-md-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Your Message<span class="required">*</span></label>
									     <textarea id="form_message" name="message" class="form-control" rows="20" cols="20" required="required" data-error="Please, leave us a message."></textarea>
										<div class="help-block with-errors"></div>
									</div>
								</div>

								<div class="col-md-6">
									<input type="submit" class="btn btn-primary contact_button" value="Get In Touch">
								</div>
							</div>
                      </form>
				</div>
				 <div class="col-md-4 bg-img1">
				 	 <!-- <img src="/image/Modern_Technical_College_8.jpg" class="img-responsive" alt="star-hospital" style="box-shadow: 1px 1px 5px #379e50;"> -->
				 </div>
			</div>
		</div>
	</div>
</section>
@endsection