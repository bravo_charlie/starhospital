@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Our Doctors
		</h5>
	</div>
</section>
<section class="bg-grey" id="doctorsearch">
	<div class="container pt-3 pb-3 h-100">
      @if(count($queries)) 
		<div class="row h-100">
    		@foreach($queries as $doctors)
			<div class="col-md-12 p-2 doctorssearch" style="margin-bottom:20px;">
			  <div class="row doctorblade" >
		       <div class="col-sm-3">
		         <div class="circular--landscape">
			       <img src="/uploads/doctor/{{$doctors->image}}" alt="image">
		        </div>
		       </div>
		     <div class="col-sm-6">
			   <h3 class="doctordet">
				 {{$doctors->name}}
			   </h3>
			    <p class="docedu">{{$doctors->education}}</p>
			     <h6>
			        @foreach($departments as $department)
			        @if($department->id == $doctors->dept_id)
			         <p>{{$department->departmentname}}</p>
			        @endif
			        @endforeach
			    </h6>
			   <div class="profile-header__expertise">
                   <h2 class="doctoredu">Expertise </h2>
                   <h6><?php echo ($doctors->speciality) ?></h6>
                   <h2 class="doctoredu">OPD Timing</h2>
            <span class="timetable">
            <?php echo("$doctors->opdtime"); ?>
            </span>
                    <button  class=" btn btn-text btn-primary detaildoctor" type="button" data-toggle="collapse" href=".multi-collapse" aria-expanded="false" aria-controls="biography edudetails">Show More Details</button>
                </div>
		    </div>
		    <div class="col-sm-3">
		      <div class="profile-header__share">
                  <ul class="plain">
                    <!-- <li>
                      <a href="mailto:?Subject= {{$doctors->name}} - {{$doctors->education}}&amp;body=http://starhospital.aeydentech.com/doctordetails/1" class="profile-share__email no-default-email-icon"><i class="fa fa-envelope" aria-hidden="true"></i>
                         Email this page
                      </a>
                  </li> -->
                  <!-- <li>
                    <a  href="javascript:window.print()" class="profile-share__print"><i class="fa fa-print" aria-hidden="true"></i>
                    Print this page
                  </a>
                </li> -->
               <li>
                <a class="accordian" data-toggle="collapse" href="#biography" role="button" aria-expanded="false" aria-controls="biography"><i class="fa fa-user"></i> Biography</a>
               </li>
                <li>
                   <a class="accordian" type="button" data-toggle="collapse" href="#edudetails" aria-expanded="false" aria-controls="edudetails"><i class="fa fa-book"></i> Education Details</a>
                 </li>
                 <li> 
        	         <a type="button" data-toggle="collapse" href="#" data-target=".multi-collapse" aria-expanded="false" aria-controls="biography edudetails"><i class="fa fa-users" aria-hidden="true"></i> View More Profile</a>
                 </li>
              </ul>
              <ul class="ul-unstyled ">
								<li>
									<a href="#" role="button" class="btn btn-sm  btn-custom searching">
										Book Appointment
									</a>
								</li>
                <li>
                  <a href="#" role="button" class="btn btn-sm  btn-custom booktele">
                    Book Tele-Consultation
                  </a>
                </li>
							</ul>
                     </div>
		          </div>
               </div>	

<div class="row">
  <div class="col-sm-12">
    <div class="collapse multi-collapse" id="biography">
      @if($doctors->biography)
      <div class="card card-body">
      	<h2 class="doctoredu">Biography</h2>
       	<h6 class="association"><?php echo ($doctors->biography) ?></h6>
      </div>
      @endif
    </div>
  </div>
  <div class="col-sm-12">
    <div class="collapse multi-collapse" id="edudetails">
      <div class="card card-body">
        <div class="row">
			 <div class="col-sm-6">
        @if($doctors->edudetails)
				<h2 class="doctoredu">Education</h2>
				<h6 class="association"><?php echo ($doctors->edudetails) ?></h6>
          @endif
          @if($doctors->language)
				 <h2 class="doctoredu">Language</h2>
				 <h6 class="association"><?php echo ($doctors->language) ?></h6>
         @endif
			</div>
      @if($doctors->membershipassociate)
			<div class="col-sm-6">
				<h4 class="doctoredu">Membership Associations</h4>
				<h6 class="association"><?php echo ($doctors->membershipassociate ) ?></h6>
			</div>
      @endif
		     @if($doctors->description)
         <div class="col-sm-12">
           <h4 class="doctoredu">Publication</h4>
           <h6 class="association"><?php echo ($doctors->description ) ?></h6>
         </div>
         @endif
       </div>
      </div>
    </div>
  </div>
</div>
<div class="collapse" id="collapseExample">
  <div class="card card-body">
   <div class="col-sm-12">
   	<?php echo ($doctors->biography) ?>
   </div>
 </div>
</div>
		</div>
			@endforeach
    	</div>
    	@else
    	<h2><strong>Opps</strong> No doctors found</h2>
    	<a href="/Doctor"><buttom type="submit" class="btn btn-primary">Go Back</buttom></a>
		@endif
  </div>
</section>
<script src="https://code.jquery.com/jquery-1.9.1.min.js"></script>
<script type="text/javascript">
    jQuery(document).ready(function() {
 
    jQuery('[href=".multi-collapse"]').click(function() {

     jQuery(this).toggleClass( "active" );
     if (jQuery(this).hasClass("active")) {
       jQuery(this).text("Show Less Details");
         } else {
       jQuery(this).text("Show More Details");
         }
       });

     });
</script>
@endsection


