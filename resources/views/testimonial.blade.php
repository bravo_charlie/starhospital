@extends('layouts.app')

@section('content')
<section class="parallax">
  <div class="container parallax-content">
    <h5>
     Testimonial
    </h5>
  </div>
</section>
<section class="bg-grey">
   <div class="container pt-3 pb-3 h-100">
      <input type="hidden" id="txtCurrentPage" value="1" />
       <input type="hidden" id="txtTotalPages" value="4" />
        <div class="row h-100" id="newsToAppend">
          @foreach($testimonials as $testimonial)
                <div class="col-lg-4 col-md-6">
                    <div class="card mb-4 box-shadow crop">
                       <a href="#">
                        <img class="card-img imagenews" src="/uploads/testimonial/{{$testimonial->image}}">
                       </a>
                        <div class="card-body">
                            <div class="contentpart">
                               <a href="#">
                                 <h5 class="txt-blue">
                                   {{$testimonial->name}}
                               </h5>
                            </a>
                           <?php 
                             $excerpt = $testimonial->description;
                             $the_str = substr($excerpt, 0, 150);
                              echo $the_str; 
                              ?>...
                            </div>
                              <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="/readmore/{{$testimonial->id}}" class="btn btn-custom btn-sm">
                                        Know More
                                    </a> 
                                </div>
                                <small class="text-muted"><i class="fa fa-calendar"></i>{{$testimonial->created_at->format('M d , Y')}}</small>
                            </div> 
                        </div>
                    </div>
                </div>
                @endforeach
           </div>
         </div>
  </section>
@endsection