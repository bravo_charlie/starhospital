@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Our Doctors
		</h5>
	</div>
</section>
<section class="bg-blue inner-header-bar">
	<div class="container">
		<div class="row">
			<div class="col-4 col-xs-6 searchBoxNewdoc">
				<div class="form-group search">
					<label><h2 class="selectdoc">Department</h2>
						<select name="dept_id" class="form-control" id="deptDdl">
							<option value="">All Department</option>
							@foreach($departments as $department)
							<option value="{{$department->id}}">{{$department->departmentname}}</option>
							@endforeach
						</select>
					</label>
				</div>
			</div>
			<div class="col-4 col-xs-6 searchBoxNewdoc">
				<div class="form-group" style="margin:0 auto;">
					<label><h2 class="selectdoc">Select Doctor</h2>
						<input type="text" class="form-control searchareainput"  onchange="callAjax(event)" id="searchDr" name="search" placeholder="Search for a Doctor"> 
						<span class="input-group-btn">
						</span>
					</label>
				</div>
			</div>
			<div class="col-4 col-xs-6 searchBoxNewdoc" style="margin-top:24px;">
				<input type="submit" class="btn btn-primary contact_button searchareainput" onclick="callAjax(event)" value="Search" >
			</div>
		</div>

	</div>
</section>
<script type="text/javascript">
function callAjax(evt, id){

	var dept_id=$( "#deptDdl" ).val();
	var searchDr=$( "#searchDr" ).val();
        	//console.log(searchDr);
        	//console.log(dept_id);
        	$.ajaxSetup({
        		headers: {
        			'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        		}
        	});
        	$.ajax({
        		type: 'GET',
        		beforeSend: function(){
        			$('#loader').css("visibility", "visible");
        		},
        		url: '/search',
        		data: {'dept_id' : dept_id,
        		'searchDr': searchDr
        	},
        	success: function (response) {
        		
        		$('#doctorsearchContent').show();
        		$('#doctorsearchContent').html(response);
        		$('#doctorSearchNormal').hide();
        		
        	},
        	complete: function(){
        		$('#loader').css("visibility", "hidden");
        	}
        });
        	
        }
        </script>

        <div id="loader" class="ajax-loader">
        	<img src="/image/loading.gif" class="img-responsive" />
        </div>

        <div id="doctorsearchContent" class="col-md-12 p-4 bg-white" >
        </div>

        <section class="bg-grey" id="doctorSearchNormal">
        	<div class="container pt-3 pb-3 h-100">
        		<div class="row h-100">
        			<div class="col-md-12 p-2 doctors-list">
        				<div class="row doctorblade">
        					@foreach($doctors as $doctor)
        					<div class="col-lg-4 col-md-6" style="margin-top:20px;">
        						<div class="card-desk bg-white">
								<div class="img-div">
        							<a href="/doctordetails/{{$doctor->id}}">
        								<img src="uploads/doctor/{{$doctor->image}}" alt="image">
        							</a>
									</div>
        							<div class="card-header-content">
        								<a href="/doctordetails/{{$doctor->id}}">
        									<h2>
        										<b>{{$doctor->name}} </b>
        									</h2>
        								</a>

        								<h6>
        									@foreach($departments as $department)
        									@if($department->id == $doctor->dept_id)
        									{{$department->departmentname}}
        									@endif
        									@endforeach
        								</h6>
        								<hr>
        							</div>
        							<ul class="ul-unstyled list-btn">
        								<li>
        									<a href="/doctordetails/{{$doctor->id}}" role="button" class="btn btn-sm btn-lined btn-sm">
        										View Profile
        									</a>

        								</li>

        								<li>
        									<a href="#" role="button" class="btn btn-sm  btn-custom">
        										Book Appointment
        									</a>
        								</li>
        							</ul>
        						</div>
        					</div>
        					@endforeach
        				</div>
        				{!! $doctors->links() !!}
        			</div>
        		</div>
        	</div>
        </section>

       @endsection