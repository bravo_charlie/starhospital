@extends('layouts.app')

@section('content')
  

<section class="parallax">
	<div class="container parallax-content">
   
		<h5>
			About Us
		</h5>

</div>
</section>


<section class="abouttopic">
<div class="innerpage aboutpage">
   <div class="container">
    <div class="row">
          <div class="col-md-12">
          <div class="about-content">
          <p>Star Hospital was established in 2007 as a 50-bedded, multispecialty hospital. On May 
          	30, 2017, the hospital shifted to its own premises in Sanepa-2, Lalitpur. It is the first 
          	hospital to meet the standards set by the Ministry of Health and Population of Nepal. 
          	Our 100-bedded hospital has 22 specialty centres and features state-of-the-art modular
          	 operation theatres.

            Our long-term vision is to establish a well-equipped multi-specialty hospital which would
             be able to give quality services at reasonable cost under the same umbrella with a motto
              of “Compassionate Care With Comfort”.
           </p>
          <p>Star Hospital also owns and manages two educational institutions: Modern Technical 
            College and Star Academy.Modern Technical College offers Bachelor's in medical Laboratory 
            Technology and produces proficient bachelor-level lab technicians,and Star Academy offers PCL 
            Nursing course.
        </p>
        <p>&nbsp;</p>
            
    </div>
  </div>
 </div>
 <div class="row">
  <div class="col-sm-3">
  </div>
 <div class="col-md-6">
    <img src="/image/Modern_Technical_College_8.jpg" class="img-responsive" alt="star-hospital" style="box-shadow: 1px 1px 5px #379e50; margin-top: 16px;">
  </div>
  <div class="col-sm-3">
  </div>
</div>
</div>
</div>
</section>




@endsection