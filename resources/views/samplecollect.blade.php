@extends('layouts.app')

@section('content')
  

<section class="parallax">
	<div class="container parallax-content">
   
		<h5>
			Sample Collection
		</h5>

</div>
</section>


<section class="abouttopic">
<div class="innerpage aboutpage">
   <div class="container">
    <div class="row">
      <h1 class="entry-title">Sample collection form for suspected covid-19</h1>
      <div class="col-md-6 col-xs-12 covid">
        <a href="/uploads/pdf/PCR-form-for-Diplomatic-Mission.pdf">
         <img class="covidtest" src="image/diagnostic.png">
        </a>
     </div>
     <div class="col-md-6 col-xs-12 covid">
      <a href="/uploads/pdf/Star-PCR-Form-for-Hospital.pdf">
        <img class="covidtest" src="image/sampleform.png">
      </a>
    </div>
  </div>
</div>
</div>
</section>




@endsection