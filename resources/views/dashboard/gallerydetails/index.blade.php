@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/gallerydetails">Gallery Details</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="row m-t-30">
		<div class="col-md-12">
			<a style="margin-bottom: 10px;" href="/home/gallerydetails/create" class="btn btn-primary">Add Gallery Image</a>
			<!-- DATA TABLE-->
			<div class="table-responsive m-b-40">
				<table class="table table-borderless table-data3">
					<thead>
						<tr>
							<th>Id</th>
							<th>Gallery Name</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($gallerydetails as $galldetails)
						<tr style="border-bottom:2px solid grey;">
							<td>{{ $loop->iteration }}</td>
							@foreach($gallery as $galle)
							@if($galle->id == $galldetails->Gal_id)
							<td>{{$galle->name}}</td>
							@endif
							@endforeach
							<td class="process">
								<?php
								$json = ($galldetails->image);
								$arr = json_decode($json, true);
								foreach($arr as $key=>$value){
									?>
									<img src="/uploads/gallerydetails/{{$value}}">
									<?php
								}
								echo "<hr>";
								?>
							</td>
							<td>
								<form method="post" action="{{route('gallerydetails.delete',$galldetails->id)}}">
								{{ csrf_field() }}
									{{ method_field('DELETE') }}
									<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
								</form>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>
			<!-- END DATA TABLE-->
		</div>
	</div>
</div>

<script type="text/javascript">
function makeWarning(evt){
	let result = confirm("Are you sure to Delete?");
	if(! result){
		evt.stopPropagation();
		evt.preventDefault();	
	}
}
</script>

@endsection