@extends('layouts.dashboard.admin')

@section('content')

<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/gallerydetails">Gallery Details</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">create</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/gallerydetails/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="Gal_id" class=" form-control-label">Choose Gallery</label>
				</div>
				<div class="col-12 col-md-9">
					<select name="Gal_id" required class="form-control">
						<option value="">---</option>
							@foreach($gallery as $galle)
						<option value="{{$galle->id}}">{{$galle->name}}</option>
							@endforeach
						</select>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="images[]" class="form-control-file" multiple/>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
			<button type="reset" class="btn btn-danger btn-sm">
				<i class="fa fa-ban"></i> Reset
			</button>
		</form>
	</div>
</div>
@endsection

