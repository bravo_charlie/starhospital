@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/news">news</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row m-t-30">
		<div class="col-md-12">
			<a style="margin-bottom: 10px;" href="/home/news/create" class="btn btn-primary">Add New News</a>
			<!-- DATA TABLE-->
			<div class="table-responsive m-b-40">
				<table class="table table-borderless table-data3">
					<thead>
						<tr>
							<th>Id</th>
							<th> News Name</th>
							<th>Description</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($news as $news)
						<tr style="border-bottom:2px solid grey;">
							<td>{{ $loop->iteration }}</td>
							<td>{{$news->newstitle}}</td>
							<td><?php echo ($news->description ) ?></td>
							<td class="process">
								<img src="/uploads/news/{{$news->image}}"></td>
								<td class="actionbuttom">
									<a href="{{route('news.edit', $news->id)}}">
										<button class="btn btn-primary make-btn action">Edit</button></a>|
									<form method="post" action="{{route('news.delete',$news->id)}}">
									{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>

	<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
	</script>

	@endsection