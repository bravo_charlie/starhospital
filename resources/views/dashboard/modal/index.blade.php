@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/modal">Modal</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row m-t-30">
		<div class="col-md-12">
			<a style="margin-bottom: 10px;" href="/home/modal/create" class="btn btn-primary">Add New Modal</a>
			<!-- DATA TABLE-->
			<div class="table-responsive m-b-40">
				<table class="table table-borderless table-data3">
					<thead>
						<tr>
							<th>Id</th>
							<th>Modal Heading</th>
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($modals as $modal)
						<tr style="border-bottom:2px solid grey;">
							<td>{{ $loop->iteration }}</td>
							<td>{{$modal->modalname}}</td>
							<td class="process">
								<img src="/uploads/modal/{{$modal->image}}"></td>
								<td class="actionbuttom">
									<a href="{{route('modal.edit', $modal->id)}}">
										<button class="btn btn-primary make-btn action">Edit</button></a>|
									<form method="post" action="{{route('modal.delete',$modal->id)}}">
									{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
									</form> |
									<form action="/home/modal/edit/{{$modal->id.'-'.$modal->active}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
									{{ csrf_field() }}
									<button type="submit"  class="btn btn-info">{{($modal->active ==1 ) ? 'Active':'Inactive'}}</button>
									</form>
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>

	<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
	</script>

	@endsection