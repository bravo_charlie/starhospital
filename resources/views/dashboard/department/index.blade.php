@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/department">Departments</a>
				</li>
			</ul>
		</div>
	</div>
	<div class="row m-t-30">
		<div class="col-md-12">
			<a style="margin-bottom: 10px;" href="/home/department/create" class="btn btn-primary">Add New Department</a>
			<!-- DATA TABLE-->
			<div class="table-responsive m-b-40">
				<table id="example" class="table table-borderless table-data3">
					<thead>
						<tr>
							<th>Id</th>
							<th> Department Name</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($department as $departments)
						<tr style="border-bottom:2px solid grey;">
							<td>{{ $departments->id }}</td>
							<td>{{$departments->departmentname}}</td>
							<td class="actionbuttom"><a href="{{route('department.edit', $departments->id)}}">
								<button class="btn btn-primary make-btn action">Edit</button></a>|
									<form method="post" action="{{route('department.delete',$departments->id)}}">
									{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
									</form>
								</td>
							</tr>

							@endforeach

						</tbody>
					</table>
					{!! $department->links() !!}
				</div>
				
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>
	
	<script type="text/javascript">
	function makeWarning(evt){
		let result = confirm("Are you sure to Delete?");
		if(! result){
			evt.stopPropagation();
			evt.preventDefault();	
		}
	}
  </script>
  <script>
  $(document).ready(function() {
    $('#example').DataTable();
} );
</script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">s
@endsection
