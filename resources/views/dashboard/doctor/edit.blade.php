@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/news">news</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">Edit</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/doctor/edit/{{$doctors->id}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="name" class=" form-control-label">Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="name" value="{{$doctors->name}}" class="form-control">

				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="dept_id" class=" form-control-label">Choose Department</label>
				</div>
				<div class="col-12 col-md-9">
					<select name="dept_id" required class="form-control">
											
						@foreach($departments as $department)
						@if($department->id == $doctors->dept_id)
						<option value="{{$department->id}}">{{$department->departmentname}}</option>
						@endif
					    @endforeach
					    
						@foreach($departments as $department)
						@if($department->id != $doctors->dept_id)
						<option value="{{$department->id}}">{{$department->departmentname}}</option>
						@endif
					    @endforeach
						</select>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
					<p class="text-danger">Warning:Preffered image size should be 670*500 px.</p>
				</div>
				<div class="col-12 col-md-9 process">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
					<img src="/uploads/doctor/{{$doctors->image}}" alt="{{$doctors->name}}">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Publication</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="description" id="description" rows="9"  class="form-control ckeditor">{{$doctors->description}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="education" class=" form-control-label">Education</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="education" id="education" rows="9"  class="form-control">{{$doctors->education}}</textarea>
				</div>
			</div>

			<div class="row form-group">
				<div class="col col-md-3">
					<label for="speciality" class=" form-control-label">Speciality</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="speciality" id="speciality" rows="9"  class="form-control ckeditor">{{$doctors->speciality}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="biography" class=" form-control-label">Biography</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="biography" id="biography" rows="9"  class="form-control ckeditor">{{$doctors->biography}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="edudetails" class=" form-control-label">Education Details</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="edudetails" id="edudetails" rows="9"  class="form-control ckeditor">{{$doctors->edudetails}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="membershipassociate" class=" form-control-label">Memberships Associations</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="membershipassociate" id="membershipassociate" rows="9"  class="form-control ckeditor">{{$doctors->membershipassociate}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="language" class=" form-control-label">Language</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="language" id="language" rows="9"  class="form-control ckeditor">{{$doctors->language}}</textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="opdtime" class=" form-control-label">OPD Timing</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="opdtime" id="opdtime" rows="9"  class="form-control ckeditor">{{$doctors->opdtime}}</textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Update
			</button>
		</form>
	</div>
	<div class="card-footer">

	</div>
</div>
@endsection