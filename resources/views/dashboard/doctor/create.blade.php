@extends('layouts.dashboard.admin')

@section('content')

<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item">
					<a href="/home/news">Doctor</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">create</li>
			</ul>
		</div>
	</div>
	@if($errors->any())
	@foreach($errors->all() as $error)
	<ul>
		<li>{{$error}}</li>
	</ul>
	@endforeach
	@endif
	<div class="card-body card-block">
		<form action="/home/doctor/create" method="POST" class="form-horizontal" enctype="multipart/form-data">
		{{ csrf_field() }}
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="name" class=" form-control-label">Doctor Name</label>
				</div>
				<div class="col-12 col-md-9">
					<input type="name" id="name" name="name" placeholder="Enter Doctor Name..." class="form-control">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="dept_id" class=" form-control-label">Choose Department</label>
				</div>
				<div class="col-12 col-md-9">
					<select name="dept_id" required class="form-control">
						<option value="">---</option>
							@foreach($departments as $department)
						<option value="{{$department->id}}">{{$department->departmentname}}</option>
							@endforeach
						</select>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="image" class=" form-control-label">Image input</label>
					<p class="text-danger">Warning:Preffered image size should be 670*500 px.</p>
				</div>
				<div class="col-12 col-md-9">
					<input type="file" id="image" accept="image/png, image/jpg, image/jpeg" name="image" class="form-control-file">
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="education" class=" form-control-label">Education</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="education" id="education" rows="4" class="form-control"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="speciality" class=" form-control-label">Speciality</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="speciality" id="speciality" rows="9" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="opdtime" class=" form-control-label">OPD Timing</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="opdtime" id="opdtime" rows="3" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="biography" class=" form-control-label">Biography</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="biography" id="biography" rows="3" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="edudetails" class=" form-control-label">Education Details</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="edudetails" id="edudetails" rows="3" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="membershipassociate" class=" form-control-label">Membership Associations</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="membershipassociate" id="edudetails" rows="3" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="language" class=" form-control-label">Languages</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="language" id="edudetails" rows="3" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<div class="row form-group">
				<div class="col col-md-3">
					<label for="description" class=" form-control-label">Publication</label>
				</div>
				<div class="col-12 col-md-9">
					<textarea name="description" id="description" rows="9" class="form-control ckeditor"></textarea>
				</div>
			</div>
			<button type="submit" class="btn btn-primary btn-sm">
				<i class="fa fa-dot-circle-o"></i> Submit
			</button>
			<button type="reset" class="btn btn-danger btn-sm">
				<i class="fa fa-ban"></i> Reset
			</button>
		</form>
	</div>
</div>
@endsection