@extends('layouts.dashboard.admin')

@section('content')
<!-- PAGE CONTAINER-->
<div class="container card">
	<div class="card-header">
		<div class="au-breadcrumb-left">
			<span class="au-breadcrumb-span">You are here:</span>
			<ul class="list-unstyled list-inline au-breadcrumb__list">
				<li class="list-inline-item">
					<a href="/home">Home</a>
				</li>
				<li class="list-inline-item seprate">
					<span>/</span>
				</li>
				<li class="list-inline-item active">
					<a href="/home/doctor">Doctor</a>
				</li>
			</ul>
		</div>
	</div>

	<div class="row m-t-30">
		<div class="col-md-12">
			<a style="margin-bottom: 10px;" href="/home/doctor/create" class="btn btn-primary">Add New Doctor</a>
			<!-- DATA TABLE-->
			<div class="table-responsive m-b-40">
				<table id="example" class=" display table table-borderless table-data3">
					<thead>
						<tr>
							<th>Id</th>
							<th>Doctor Name</th>
							
							<th>Department Name</th>
							<th>Doctor Education</th>
							<!-- <th>Doctor Speciality</th> -->
							<!-- <th>Doctor OPDTime</th>	 -->
							<!-- <th>Doctor Publication</th> -->
							<th>Image</th>
							<th>Action</th>
						</tr>
					</thead>
					<tbody>
						@foreach($doctors as $doctor)
						<tr style="border-bottom:2px solid grey;">
							<td>{{ $loop->iteration }}</td>
							<td>{{$doctor->name}}</td>
							
							@foreach($departments as $department)
							@if($department->id == $doctor->dept_id)
							<td>{{$department->departmentname}}</td>
							@endif
							@endforeach
							<td>{{$doctor->education}}</td>
							<!-- <?php echo ($doctor->active)."hell" ?></td>v -->
							<!-- <td><?php echo ($doctor->opdtime) ?></td> -->
							<!-- <td><?php echo ($doctor->description ) ?></td> -->
							<td class="process">
							 <img src="/uploads/doctor/{{$doctor->image}}">
							</td>
								<td class="actionbuttom"><a href="{{route('doctor.edit', $doctor->id)}}">
									<button class="btn btn-primary make-btn action">Edit</button></a>|
									<form method="post" action="{{route('doctor.delete',$doctor->id)}}">
									{{ csrf_field() }}
										{{ method_field('DELETE') }}
										<button type="submit" onclick="makeWarning(event)" class="btn btn-danger">Delete</button>
									</form> || 
									<form action="/home/doctor/edit/{{$doctor->id .'-'.$doctor->is_listed}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
									{{ csrf_field() }}
									<button type="submit" class="btn btn-primary btn-info">{{($doctor->is_listed ==1 ) ? 'Active':'Inactive'}}</button>
									</form>
								
								
								</td>
							</tr>
							@endforeach
						</tbody>
					</table>
				</div>
				<!-- END DATA TABLE-->
			</div>
		</div>
	</div>

<script type="text/javascript">
function makeWarning(evt){
	let result = confirm("Are you sure to Delete?");
	if(! result){
		evt.stopPropagation();
		evt.preventDefault();	
	}
}
</script>
	

	@endsection