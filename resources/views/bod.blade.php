@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
   	<h5>
	 Board Of Directors
    </h5>
   </div>
</section>
<section class="abouttopic">
	<div class="aboutpage bod">
	 <div class="container">
		<div class="row">
			<h5>The Board of Directors comprises eleven members, who develop hospital policies, plans and programs.</h5>
			<div class="col-sm-12">
			<ul>
				<li class="bodlist">Mr. Kishore Kumar Maharjan (Executive Chairman)</li>
				<li class="bodlist">Dr. Shail Rupakheti (Medical Director)</li>
				<li class="bodlist">Mr. Chandra Maharjan</li>
				<li class="bodlist">Er. Bashant Chandra Marahatta</li>
				<li class="bodlist">Mr. Nav Raj Pokhrel</li>
				<li class="bodlist">Mr. Arun Lal Shrestha</li>
				<li class="bodlist">Mr. Tirtha Lal Maharjan</li>
				<li class="bodlist">Mr. Shanta Man Maharjan</li>
				<li class="bodlist">Mr. Kiran Maharjan</li>
				<li class="bodlist">Dr. Rajeev Shrestha</li>
				<li class="bodlist">Dr. Shailesh Pradhan</li>
			</ul>
			</div>
		</div>
	</div>
 </div>
</section>


@endsection