@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
   
		<h5>
			Chairman Message
		</h5>

</div>
</section>


<section class="content-style-1 section-margin ">
    <div class="container">
        <div class="row">
        	<div class="col-md-6">
                <img src="/image/Kishore-Maharjan-1.jpg" class="img-responsive" alt="star-hospital" style="box-shadow: 1px 1px 5px #379e50; margin-top: 16px;">
            </div>
            <div class="col-md-6">
                <div class="inner-content-header">
                    <h1>
                        
                    </h1>
                    
                </div>
                <p>
                  Dear friends and stakeholders of Star Hospital,<br>
                 Jajwolapa – Namaskar !<br>
                 I have this special pleasure of extending to you all very warm greetings from me 
                 and the entire Star Hospital Family. In personal capacity, I am pleased to be welcomed 
                 by the Hospital’s Board of Directors to chair the hospital and move forward. Further, 
                 I find greater pleasure in being welcomed so warmly by the entire Star Hospital family 
                 comprised of the staff members, doctors, other medical personnel including the 
                 shareholders.<br>
                 Operating from it’s own lavish modern hospital building, this is indeed a golden 
                 feather on our crown. With the induction of large number of shareholders from the
                  local Jyapu community, primarily from the city of Lalitpur, the Capital of the 
                  company has increased to impressive level that gives us the strength to move forward
                   with a whole range of modern hospital services.<br>

                  Star Hospital is now all set to shine brighter with introduction of numerous services,
                   excelling in orthopedics, neuro-sciences, gastroenterology, cardiology, gynecology, 
                   pediatrics, internal medicine, etc. Over the last few recent weeks, the hospital has 
                   been growing in leaps and bounds with increasing footfalls and occasionally having 
                   difficulty in providing hospital bed to the needy patients. It is indeed a moment to be 
                   proud as we build our impression deeper into the society and instill strong sense of 
                   confidence in it – the trust that we all always crave for.<br>

                   What’s so unique about Star Hospital is the warmth and patient centric staff we have,
                    ever caring and ever understanding each patient’s needs. With the trust, affection and
                     support of all our stakeholders and not least the entire community we serve, Star
                      Hospital will shine ever brighter. Come, be part of the Star !<br>

                    My sincerest gratitude to all.<br>

                  Jai Star.<br>

       <strong>    Kishore Kumar Maharjan, Chairman  </strong>
	
		        </p>
             </div>
       </div>
    </div>
</section>




@endsection