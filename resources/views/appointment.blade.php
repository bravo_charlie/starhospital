@extends('layouts.app')

@section('content')

<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Appointment
		</h5>
	</div>
</section>
<section class="contact-form">
	<div class="container">
		<div class="col-md-12 mt-3 bg-white  shadow-box  md-3">
                   @if ($errors->any())
					<div class="alert alert-danger">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<ul>
							@foreach ($errors->all() as $error)
							<li>{{ $error }}</li>
							@endforeach
						</ul>
					</div>
					@endif
					@if (Session::has('success'))
					<div class="alert alert-success text-center">
						<a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
						<p>{{ Session::get('success') }}</p>
					</div>
					@endif
			<div class="row">
				<div class="col-md-12 bg-white pd-3">
					
					<form action="/sendemail/bookappointment" id="FormCreate" method="post">
						{!! csrf_field() !!}
						
						<div class=" container formlayout">
							<div class="col-md-12">
								<h1>
									Book Appointment
									<span class="pull-right">
										<i class="fa fa-paper-plane"></i>
									</span>
								</h1>
								<hr>
							</div>
							<h4>Appointment's Details</h4>
							<div class="row">
								<div class="form-group col-md-12">
									<label for="exampleInputEmail1">Departments <span class="required">*</span></label>
									<select class="form-control select2-single" data-val="true" data-val-number="The field DoctorDepartmentId must be a number." data-val-required="Deparment Name is required!" id="DepartmentId" name="DoctorDepartmentname">
									<option selected="selected">---select departments----</option>
                                     @foreach($departments as $department)
						             <option value="{{$department->departmentname}}">{{$department->departmentname}}</option>
							         @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
								</div>
								<div class=" col-md-12">
									<label for="exampleInputEmail1">Doctors <span class="required">*</span></label>
									<select class="form-control select2-single" data-val="true" data-val-required="Doctor Name is required!" id="DoctorId" name="Doctorname">
									<option selected="selected">---select doctors----</option>
                                     @foreach($doctors as $doctor)
						             <option value="{{$doctor->name}}">{{$doctor->name}}</option>
							         @endforeach
                                    </select>
                                    <div class="help-block with-errors"></div>
								</div>
                                <div class="col-md-12">
                                  	<label for="exampleInputEmail1">Appointment Date<span class="required">*</span></label>
									<input id="datetimepicker" type="date" name="appointmentdate" class="form-control"  required="required" data-error="Date is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="col-md-12">
									<div class="form-group">
										<label for="exampleInputEmail1">Reason For Appointment <span class="required">*</span></label>
									     <textarea id="form_message" name="reason" class="form-control" rows="20" cols="20" required="required" data-error="Please, leave us a message."></textarea>
										<div class="help-block with-errors"></div>
									</div>
								</div>
							</div>
							<hr style="border:2px solid grey;">
							 <h4>Patient's Details</h4>
							<div class="row">
                       		<div class="form-group col-md-6">
									<input id="form_name" type="text" name="name" class="form-control" placeholder="Your Name *" required="required" data-error="Name is required.">
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group col-md-6">
									<input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
									<div class="help-block with-errors"></div>
								</div>

								<div class="form-group col-md-6">
									<input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile *" required="required" data-error="PhoneNumber is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="patient_age" type="text" name="age" class="form-control" placeholder="Your Age *" required="required" data-error="Age is required.">
									<div class="help-block with-errors"></div>
								</div>
								<div class="form-group col-md-6">
									<input id="nationality" type="text" name="nation" class="form-control" placeholder="Your Nationality *" required="required" data-error="Nationality is required.">
									<div class="help-block with-errors"></div>
								</div>

                                <div class="col-md-6">
                                    <label for="exampleInputEmail1">Gender <span class="required">*</span></label>
                                    <label class="check-style">
                                        Male
                                        <input checked="checked" id="Gender" name="Gender" type="radio" value="Male" />
                                     <span class="checkmark"></span>
                                    </label>
                                    <label class="check-style">
                                        Female
                                        <input id="Gender" name="Gender" type="radio" value="Female" />
                                        <span class="checkmark"></span>
                                    </label>
                                </div>

								<div class="col-md-6">
									<input type="submit" class="btn btn-primary contact_button" value="Send message">
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</section>
<script type="text/javascript">
$('#datetimepicker').datetimepicker({
    format: 'yyyy-mm-dd'
});</script>


@endsection