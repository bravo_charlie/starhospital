@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			PCR Test Request
		</h5>
	</div>
</section>
<section class="contact-form">
	<div class="container">
		<div class="col-md-12 mt-3 bg-white  shadow-box  md-3">
      @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
			<div class="row">
    	<div class="col-md-12 bg-white pd-3">
  			<form  method="post" action="/sendemail/pcr-test-request" id="FormCreate">
						 {!! csrf_field() !!}
						
              <div class=" container formlayout">
              	<div class="col-md-12">
                                <h5>
                                   Request For PCR Test
                                    <span class="pull-right">
                                        <i class="fa fa-paper-plane"></i>
                                    </span>
                                </h5>
                                <hr>
                            </div>
                 <div class="row">

                   <div class="form-group col-md-6">
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Your Name *" required="required" data-error="Name is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                     <input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
                      <div class="help-block with-errors"></div>
                  </div>
                   <div class="form-group col-md-6">
                     <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile Number*" required="required" data-error="PhoneNumber is required.">
                      <div class="help-block with-errors"></div>
                  </div>
                   <div class="form-group col-md-6">
                    <label for="exampleInputEmail1">Gender <span class="required">*</span></label>
                                    <label class="check-style">
                                        Male
                                        <input checked="checked" id="Gender" name="sex" type="radio" value="Male" />
                                     <span class="checkmark"></span>
                                    </label>
                                    <label class="check-style">
                                        Female
                                        <input id="Gender" name="sex" type="radio" value="Female" />
                                        <span class="checkmark"></span>
                                    </label>
                                    <label class="check-style">
                                        Other
                                        <input id="Gender" name="sex" type="radio" value="Female" />
                                        <span class="checkmark"></span>
                                    </label>
                      </div>
                  <div class="form-group col-md-6">
                  <input id="Patient_age" type="text" name="age" class="form-control" placeholder="Your Age *" required="required" data-error="Age is required.">
                  <div class="help-block with-errors"></div>
                </div>
        
                <div class="col-md-12">
                   <div class="form-group">
                    <textarea id="form_message" name="message" class="form-control" placeholder="Your Message *" rows="20" cols="20" required="required" data-error="Please, leave us a message."></textarea>
                      <div class="help-block with-errors"></div>
                  </div>
                 </div>

              <div class="col-md-6">
               <input type="submit" class="btn btn-primary contact_button" value="Send message">
           </div>

       </div>
   </div>
				</form>

			</div>
			
		</div>
	</div>
</div>
</section>


@endsection