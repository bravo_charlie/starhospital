@extends('layouts.app')

@section('content')
<section class="parallax">
    <div class="container parallax-content">
        <div class="row">
            <div class="col-md-12">
                <h5>
                    Our Location 
                </h5>
            </div>
        </div>
    </div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="col-md-12 h-100  ">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.1610845958103!2d85.30110771506148!3d27.681415782802656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb183807e7e43f%3A0xa40f5104ad797baf!2sStar%20Hospital!5e0!3m2!1sen!2snp!4v1582794993162!5m2!1sen!2snp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
        </div>
    </div>
</section>
@endsection