@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Our Gallery
		</h5>
	</div>
</section>
<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100">
            <div class="col-md-12 p-2 bg-white shadow-box md-2">
                <h5 class="txt-blue">
                    Star Hospital
                </h5>
                <div class="photo-gallery-wrapper row mt-3 ">
                    @foreach($gallery as $galle)
                    <div class="col-lg-3 col-md-4 mb-sm">
                        <div class="media-content">
                            <a href="/gallerydetails/{{$galle->id}}">
                                <img src="/uploads/gallery/{{$galle->image}}" alt="starhospital gallery">
                                <div class="media-title-holder">
                                    <h6>{{$galle->name}}</h6>
                                </div>
                            </a>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>




@endsection