@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Events
		</h5>
	</div>
</section>
<section class="bg-grey">
   <div class="container pt-3 pb-3 h-100">
      <input type="hidden" id="txtCurrentPage" value="1" />
       <input type="hidden" id="txtTotalPages" value="4" />
        <div class="row h-100" id="newsToAppend">
           @foreach($allevents as $events)
                <div class="col-lg-4 col-md-6">
                    <div class="card mb-4 box-shadow crop">
                       <a href="/eventdetails/{{$events->id}}">
                        <img class="card-img imagenews"  alt="Card image cap" src="uploads/events/{{$events->image}}">
                       </a>
                       <div class="card-body">
                            <div class="contentpart">
                               <a href="/eventdetail/{{$events->id}}">
                                 <h5 class="txt-blue">
                                    {{$events->event_name}}
                               </h5>
                              </a>
                             <?php 
                             $excerpt = $events->event_description;
                             $the_str = substr($excerpt, 0, 150);
                              echo $the_str; 
                              ?>...
                            </div>
                             <div class="d-flex justify-content-between align-items-center">
                                <div class="btn-group">
                                    <a href="/eventdetail/{{$events->id}}" class="btn btn-custom btn-sm">
                                        Know More
                                    </a>
                                </div>
                                <small class="text-muted"><i class="fa fa-calendar"></i>{{$events->created_at->format('M d , Y')}}</small>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
           </div>
           {!! $allevents->links() !!}
         </div>
  </section>
@endsection