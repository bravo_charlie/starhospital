@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Testimonial Details
		</h5>
	</div>
</section>
<section class="bg-blue inner-header-bar">
    <div class="container">
  <div class="testimonial-img">
            <img class="d-block img-fluid" src="/uploads/testimonial/{{$testimonialview->image}}" alt="testimonial-img">
        </div>
  </div>
</section>

<section class="bg-grey">
    <div class="container pt-3 pb-3 h-100">
        <div class="row h-100">
            <div class="col-md-12 p-2 bg-white shadow-box md-2">
                <div class="article-listing">
                  <div class="article-content">
                        <h5><strong>{{$testimonialview->name}}</strong></h5>
                          <span>
                            <p style="text-align: justify;">
	                            <span style="font-size:14px;">
	                            	<span style="font-family:arial,helvetica,sans-serif;"><?php echo ($testimonialview->description ) ?></span>
	                            </span>
	                        </p>
                        </span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection