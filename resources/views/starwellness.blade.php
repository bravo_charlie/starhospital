@extends('layouts.app')

@section('content')
  

<section class="parallax">
	<div class="container parallax-content">
   
		<h5>
			Star Wellness
		</h5>

</div>
</section>


<section class="abouttopic">
<div class="innerpage aboutpage">
   <div class="container">
    <div class="row">
          <div class="col-md-6">
          <div class="about-content">
            <h4>STAR WELLNESS CENTER </h4>
          <p class="starwellness">
            Growing sedentary lifestyle, increasing stress levels at work place and unhealthy food habits take a toll on our body and increases the risk of health disorders such as Diabetes, Obesity, Thyroid, Liver and Kidney diseases. Star Wellness Center provides health checkup packages that are specifically designed with a vision of helping people lead a healthy life. It checks for the vital parameters which help in identifying the health risks to the major organs and significantly reduce the pace of their progression. Each health checkup plan is carefully worked on, in close liaison with our physicians, internal medicine experts and respective doctors in the various faculties of medicine.
            Come experience Star Care in its friendly and comfortable Wellness Center that is located at a designated place, away from the sick people.
           </p>
           <a href="http://starhospitallimited.com/magazine" target="_blank" class="btn btn-custom">Preventive Health Package</a>
           
         </div>
       </div>
   <div class="col-md-6">
    <img src="/image/Experiencing-Wellness-In-Every-Part-Of-Life.png" class="img-responsive" alt="star-hospital" style="box-shadow: 1px 1px 5px #379e50; margin-top: 16px;">
  </div>
 
</div>
</div>
</div>
</section>




@endsection