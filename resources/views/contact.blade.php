@extends('layouts.app')

@section('content')
<section class="parallax">
	<div class="container parallax-content">
		<h5>
			Contact Us
		</h5>
	</div>
</section>
<section class="contact-form">
	<div class="container">
		<div class="col-md-8 mt-3 bg-white  shadow-box  md-3" style="margin: 0 auto;margin-top: 46px !important;">
      @if ($errors->any())
            <div class="alert alert-danger">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
            @endif
            @if (Session::has('success'))
            <div class="alert alert-success text-center">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
                <p>{{ Session::get('success') }}</p>
            </div>
            @endif
			<div class="row">
    	<div class="col-md-12 bg-white pd-3">
  			<form  method="post" action="/sendemail/send" id="FormCreate">
						 {!! csrf_field() !!}
						
              <div class=" container formlayout">
                  <div class="row">
              

                   <div class="form-group col-md-6">
                    <input id="form_name" type="text" name="name" class="form-control" placeholder="Your Name *" required="required" data-error="Name is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                    <input id="form_email" type="email" name="email" class="form-control" placeholder="Your Email *" required="required" data-error="Valid email is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                  <div class="form-group col-md-6">
                     <input id="form_address" type="text" name="address" class="form-control" placeholder="Your Address *" required="required" data-error="Address is required.">
                      <div class="help-block with-errors"></div>
                  </div>
                   <div class="form-group col-md-6">
                     
                      <input id="form_phone" type="text" name="phone" class="form-control" placeholder="Your Mobile *" required="required" data-error="PhoneNumber is required.">
                      <div class="help-block with-errors"></div>
                  </div>

                 

                  <div class="col-md-12">
                   <div class="form-group">
                      
                      <textarea id="form_message" name="message" class="form-control" placeholder="Your Message *" rows="20" cols="20" required="required" data-error="Please, leave us a message."></textarea>
                      <div class="help-block with-errors"></div>
                  </div>
                 </div>

              <div class="col-md-6">
               <input type="submit" class="btn btn-primary contact_button" value="Send message">
           </div>

       </div>
       </div>
   </div>
				</form>

			</div>
		
		</div>
      
	</div>
</div>
<div style="margin-top:20px;">
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3533.1610845958103!2d85.30110771506148!3d27.681415782802656!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x39eb183807e7e43f%3A0xa40f5104ad797baf!2sStar%20Hospital!5e0!3m2!1sen!2snp!4v1582794993162!5m2!1sen!2snp" width="100%" height="250" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
    </div>
</section>


@endsection