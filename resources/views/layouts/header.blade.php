<?php 
 use App\Service;
 $service = Service::all();
 ?>

 <div class="top-link container-fluid bg-blue">
  <div class="row">
    <div class="col-md-6 servicetype">
      <marquee behavior="scroll" scrolldelay="200" 
           onmouseover="this.stop();"
           onmouseout="this.start();">24 HOURS EMERGENCY SERVICES COVERED BY MDGP CONSULTANTS
      </marquee>

      <!-- <i class="fa fa-ambulance" aria-hidden="true"></i> 24 Hours Emergency & Ambulance Service:
      1134 -->
    </div>
    <div class="col-md-6 menutopperr">
      <ul class="navbar-nav ml-auto top-header" style="text-align:center;">
      <!-- <li class="nav-item menutopper" style="background:none;">
         <a class="nav-link" href="https://starhospitallimited.com/newsdetails/7">Price List</a> 
         <a class="nav-link" href="https://starhospitallimited.com/newsdetails/10">AGM Notice</a> 
        </li> -->
        <li class="nav-item menutopper" style="background:none;">
          <a class="nav-link" href="/News">News/Events</a>
        </li>
        <li class="nav-item menutopper" style="background:none;">
          <a class="nav-link"href="/GetDirection">Our Location</a>
        </li>
        
        <li class="nav-item menutopper" style="background:none;">
          <a class="nav-link"href="/login">Login</a>
        </li>
        <li class="nav-item menutopper" style="background:none;">
          <a class="nav-link"href="#">+977-1-5450297</a>
        </li>
      </ul>
    </div>
  </div>
</div>

@if($errors->any() || Session::has('success'))
<div class="modal fade" id="alertBox" tabindex="-1" role="dialog" aria-labelledby="modalTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="modalLongTitle">
          @if($errors->any())
            {{ 'Errors' }}
          @elseif(Session::has('success'))
            {{ 'Success' }}
          @endif
        </h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body feedbacklayout">
        @if($errors->any())
          <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert" aria-label="close">×</a>
            <ul>
             @foreach($errors->all() as $error)
             <li>{{ $error }}</li>
             @endforeach
           </ul>
          </div>
          @endif
          @if(Session::has('success'))
          <div class="alert alert-success text-center">
            <a href="#" class="close" data-dismiss="alert" aria-label="close"></a>
            <p>{{ Session::get('success') }}</p>
          </div>
          @endif
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
      $('#alertBox').modal('show');
    });
</script>
@endif
<section class="covidresult">
<div class="container-fluid">
  <div class="row">
    <div class="col-md-4 col-xs-12 covid">
      <a href="/pcr-test-request">
        <img class="covidtest" src="/image/pcrtest.png">
      </a>
    </div>
    <div class="col-md-4 col-xs-12 covid">
    <a href="https://patient.starhospitallimited.com/login" target="_blank">
      <img class="covidtest" src="/image/covidresulttest.png">
      </a>
    </div>

    <div class="col-md-4 col-xs-12 covid">
      <a href="/sample-collection-form-for-for-suspected-covid-19">
        <img class="covidtest" src="/image/sampletest.png">
      </a>
    </div>

  </div>
</div>
</section>
<div class="top-link container-fluid header">
  <div class="row secondheader">
    <div class="col-md-4">
     <a class="navbar-brand" href="/">
      <img src="/image/starLogo-1-111x69.png"></a>
    </div>
    <div class="col-md-8">
      <div class="row secondheader1">
        <div class="col-sm-7">
          <div class="searchBoxNew">
            <form action="/finddoctor" method="POST" role="search" id="search-bar">
              {{ csrf_field() }}
              <div class="input-group doctorsearch">
               <input type="text" class="form-control doctorfind" 
               required name="search" placeholder="Search for a Doctor"
              > 
               <span class="input-group-btn searchicon">
                <button type="submit" class="btn btn-default doctoriconsearch ">
                  <i class="fa fa-search"></i>
                </button>
              </span>
            </div>
          </form>
        </div>
      </div>

      <div class="col-sm-3 col-6 brochure">
       <a href="/magazine" target="_blank" role="button" class="btn btn-custom headerappointment" style="background-color: #2e9a59 !important;align:center;">
        Brochure
       </a>
     </div>
     <div class="col-sm-2 col-6 brochure12">
       <a href="https://patient.starhospitallimited.com/opdDept" target="_blank" role="button" class="btn btn-custom headerappointment" style="background-color: #2e9a59 !important;align:center;">
         Appointment
       </a>
     </div>
   </div>

 </div>
</div>
</div>

<nav class="navbar navbar-expand-lg navbar-dark default-navbar headsection" >
  <div class="container-fluid menudrop">
   <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive"
   aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
   <span class="navbar-toggler-icon"></span>
 </button>
 <div class="collapse navbar-collapse" id="navbarResponsive">
  <ul class="navbar-nav ml-auto">
    <li class="nav-item active">
      <a class="nav-link" href="/">
        Home
        <span class="sr-only">(current)</span>
      </a>
    </li>
    <li class="nav-item dropdown menu-large">
      <a class="nav-link" data-toggle="dropdown" href="javascript:;" aria-expanded="false">
        About
      </a>
      <ul class="dropdown-menu aboutdrop">
        <li>
          <ul>
            <li class="dropmenudown"><a href="/Chairman-Message" style="color:#000;font-size:12px;">Chairman message</a></li>
            <li class="dropmenudown"><a href="/Message-From-MD" style="color:#000;font-size:12px;">Message From MD</a></li>
            <li class="dropmenudown"><a href="/bod" style="color:#000;font-size:12px;">Board Of Director</a></li>
            <li class="dropmenudown"><a href="/Mission&Vision" style="color:#000;font-size:12px;">Mission and Vision</a></li>
          </ul>
        </li>
      </ul>
    </li>
    <li class="nav-item dropdown menu-large">
      <a class="nav-link" data-toggle="dropdown" href="javascript:;" aria-expanded="false">Services</a>
      <ul class="dropdown-menu row">
        <li>
          <ul class="servicedropdown">
            <div class="row">
              <div class="col-sm-6">
                <?php $count=0;?>
                @foreach($service as $serv)
                @if($count%2 != 0)
                <li class="dropmenudown">
                  <a href="/servicedetails/{{$serv->id}}" style="color:#000;font-size:12px;">{{$serv->servicename}}</a>
                </li>
                @endif
                <?php $count++; ?>
                <?php if($count>12)
                break
                ?>
                @endforeach
              </div>
              <div class="col-sm-6">
               <?php $count=0;?>
               @foreach($service as $serv)
               @if($count%2 == 0)
               <li class="dropmenudown">
                <a href="/servicedetails/{{$serv->id}}" style="color:#000;font-size:12px;">{{$serv->servicename}}</a>
              </li>
              @endif
              <?php $count++; ?>
              <?php if($count>12)
              break
              ?>
              @endforeach
            </div>
          </div>
        </ul>
      </li>
    </ul>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/Doctor">Doctors</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/qr-pay">QR Pay</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="/starwellness">Star Wellness</a>
  </li>
  <li class="nav-item dropdown menu-large">
      <a class="nav-link" data-toggle="dropdown" href="javascript:;" aria-expanded="false">
        Academic
      </a>
      <ul class="dropdown-menu">
        <li>
          <ul>
            <li class="dropmenudown"><a href="http://mtc.edu.np/" target="_blank" style="color:#000;font-size:14px;">Modern Techincal College</a></li>
            <li class="dropmenudown"><a href="http://www.sal.edu.np/" target="_blank" style="color:#000;font-size:14px;">Star Academy Lalitpur</a></li>
            <li class="dropmenudown"><a href="http://www.innovativecollege.edu.np/" target="_blank" style="color:#000;font-size:14px;">Innovative College of Health Science</a></li>
            
          </ul>
        </li>
      </ul>
    </li>
<li class="nav-item">
    <a class="nav-link" href="/Contact">Contact Us</a>
</li>

</ul>
<ul class=" ul-unstyled report">
  <li class="nav-item dropdown patientportal">
    <a role="button"  data-toggle="dropdown" href="javascript:;" aria-expanded="false" class=" nav-link btn btn-custom labreport" target="_blank">
      Patient Portal
    </a>
    <ul class="dropdown-menu report">
        <li>
          <ul>
            <li class="dropmenudown report lab"><a href="https://patient.starhospitallimited.com/login" target="_blank"  class="btn btn-custom labreport" style="color:#fff;font-size:14px;">My lab report</a></li>
            <li class="dropmenudown report investigation"><a href="https://patient.starhospitallimited.com/login" target="_blank"  class="btn btn-custom labreport" style="color:#fff;font-size:14px;">My investigation</a></li>
             <li class="dropmenudown report lab"><a href="https://patient.starhospitallimited.com/login" target="_blank"  class="btn btn-custom labreport" style="color:#fff;font-size:14px;">My prescrition</a></li>
         </ul>
        </li>
      </ul>
  </li>
</ul>
</div>
</div>
</nav>






