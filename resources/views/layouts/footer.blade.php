  <section class="footerplace">
      <div class="container ">
      <div class="noindex">
  
    <div class="primaryFooter section">
      <div class="footer-main" style="padding:20px 0px 0px;">
        
          <div class="row">
            <div class="col-sm-8">
          <nav class="footer-main__link-lists link-list-count-4">
         <div class="row">
          <div class="col-sm-3 itemsfoot">
        <div class="footer-main__item ">
         
          <label class="acedimics" for="footer-section-2">
            Academics and Research
           </label>

         <ul>
          <li class="footerlistitem">
            <a href="http://mtc.edu.np/" target="_blank">Modern Technical College</a>
          </li>

          <li class="footerlistitem">
            <a href="http://www.sal.edu.np/" target="_blank">Star Academy Lalitpur</a>
          </li>

         
        </ul>
     </div>
</div>
            <div class="col-sm-3 itemsfoot">

            <div class="footer-main__item ">
             
              <label for="footer-section-0">
                Quick Links
               </label>

             <ul>
              <li class="footerlistitem">
                <a href="/Doctor">Find a doctor</a>
              </li>

              <li class="footerlistitem">
                <a href="https://patient.starhospitallimited.com/" target="_blank">Book an Appointment</a>
              </li>

              <li class="footerlistitem">
                <a href="#">Check my report</a>
              </li>
              <li class="footerlistitem">
                <a href="/Contact">Make an Enquiry</a>
              </li>
             <li class="footerlistitem">
                <a href="/feedback">Feedback</a>
              </li> 
              <li class="footerlistitem">
                <a href="/News">News</a>
              </li>
            </ul>
          </div>
</div>
<div class="col-sm-3 itemsfoot">
          <div class="footer-main__item ">
            
            <label for="footer-section-1">
              Learn More
             </label>
           <ul>
            <li class="footerlistitem">
              <a href="/About">About Us</a>
            </li>

            <li class="footerlistitem">
              <a href="/GetDirection">Our Location</a>
            </li>

            </ul>
        </div>
      </div>

<div class="col-sm-3 itemsfoot">
      <div class="footer-main__item ">
       
        <label for="footer-section-3">
          Connect
        </label>

       <ul>
        <li class="footerlistitem">
          <a href="/Contact">Contact Us</a>
        </li>

        <li class="footerlistitem">
          <a href="/gallery">Gallery</a>
        </li>

        <li class="footerlistitem">
          <a href="/eventdetails">Events</a>
        </li>
      </ul>
   </div>
   </div>
   <div class="col-sm-3 itemsfoot"> 
    <div class="footer-main__item ">

      <label for="footer-section-4">
        International
      </label>

     <ul>
      <li class="footerlistitem">
        <a href="/international-patient" class="international-icon">International Patient</a>
      </li>
    </ul>
  </div>
  </div>
  <div class="col-sm-3 itemsfoot">

  <div class="footer-main__item ">
   
    <label for="footer-section-5">
      Join Our Team
  </label>

   <ul>
    <li class="footerlistitem">
      <a href="#">Careers</a>
    </li>
  </ul>
</div>
</div>
</div>
</nav>

</div>



<div class="col-sm-4">
<nav class="footer-main__social-container">
  <div class="footer-main__contact-container">
    <a class="call-us__footer " href="tel:+977-1-5450198">
      <span>Call us:</span>
      <span> +977-1-5450198</span>
    </a>
  </div>

  <div class="footer-main__item ">
        <label for="footer-social">
      FOLLOW US
     </label>
    <ul class="location">
                        <li class="social-links">
                            <a href="https://www.facebook.com/starhospital.ltd/" target="_blank">
                                <i class="fa fa-facebook-square fa-2x"></i>
                            </a>
                            <a href="https://twitter.com/starhospitalltd" target="_blank">
                                <i class="fa fa-twitter-square fa-2x"></i>
                            </a>
                            <a href="https://www.youtube.com/channel/UC3wcQ6r7De4m5dLMPQE6z2w" target="_blank">
                             <i class="fa fa-youtube-square fa-2x"></i>
                         </a>
                         <a href="https://www.instagram.com/starhospitalofficial/" target="_blank">
                            <i class="fa fa-instagram fa-2x"></i>
                        </a>
                    </li>
                </ul>
</div>
</nav>
</div>
</div>


</div>
</div>
</div>
</div>
</section>

<footer>
  <section class="lastfooter">
    <div class="container">
      <div class="row">
        <div class="col-sm-8">
          <div class="footercopyright">
          <p style="color:white;letter-spacing:1.5px;">Copyright © 2017 Star Hospital.All rights reserved.</p>
        </div>
        </div>
        <div class="col-sm-4">
           <ul class="endfoot">
                        <li class="privacy">
                            <a href="https://www.facebook.com/starhospital.ltd/" target="_blank">
                                Terms of Use
                            </a>
                            <a href="https://twitter.com/starhospitalsin?lang=en" target="_blank">
                                Privacy Policy
                            </a>
                            <a href="#" target="_blank">
                             Sitemap
                         </a>
                      </li>
                </ul>
        </div>
      </div>
    </div>
  </section>
  </footer>

