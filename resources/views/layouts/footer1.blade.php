<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="footer-widget">
                    <h4>
                        Social Media
                    </h4>
                    <ul class="location">
                        <li class="social-links">
                            <a href="https://www.facebook.com/starhospital.ltd/" target="_blank">
                                <i class="fa fa-facebook-square fa-2x"></i>
                            </a>
                            <a href="https://twitter.com/starhospitalsin?lang=en" target="_blank">
                                <i class="fa fa-twitter-square fa-2x"></i>
                            </a>
                            <a href="#" target="_blank">
                             <i class="fa fa-youtube-square fa-2x"></i>
                         </a>
                         <a href="#" target="_blank">
                            <i class="fa fa-instagram fa-2x"></i>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="footer-widget">
                <h4>
                    Location Details
                </h4>
                <ul class="location details">
                    <li>
                        <span>
                            <i class="fa fa-map-marker fa-1x" aria-hidden="true"></i>
                        </span> Sanepa Height-2, Ringroad, Lalitpur, Nepal
                    </li>
                    <li>
                        <a href="/GetDirection" class="footer-direction">
                            <span>
                                <i class="fa fa-map fa-1x" aria-hidden="true"></i>
                            </span> Get Direction
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-lg-3 col-md-6">
            <div class="footer-widget">
                <h4>
                    Contact Details
                </h4>
                <ul class="location phone">
                    <li>
                        <span>
                            <i class="fa fa-phone fa-1x" aria-hidden="true"></i>
                        </span> +977-1-5450297
                    </li>
                    <li>
                        <span>
                            <i class="fa fa-phone fa-1x" aria-hidden="true"></i>
                        </span> +977-1-5450198
                    </li>
                    <li>
                        <span>
                            <i class="fa fa-envelope-o fa-1x" aria-hidden="true"></i>
                        </span> <a href="#" target="_blank">info@starhospitallimited.com</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="col-md-3">
            <div class="footer-widget">
                <h4>
                    Useful Links
                </h4>
                <ul class="directlink">
                 <li>
                    <a href="/About">
                        About Us 
                    </a>
                </li>
                <li>
                    <a href="/eventdetails">
                        Events
                    </a>
                </li>
                <li>
                    <a href="/News">
                        News &amp; Articles
                    </a>
                </li>
                <li>
                    <a href="/centerforexcellence">
                        Center For Excellence
                    </a>
                </li>
                <li>
                    <a href="#">
                        Services 
                    </a>
                </li>
            </ul>
        </div>
    </div>
</div>
<div class="row">
    <div class="container text-center footer-copyright">
        Copyright © 2017 Star Hospital, A unit of Modern Technical College and Star Hospital. All rights reserved.
    </div>
</div>
</div>
</footer>