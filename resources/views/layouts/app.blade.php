<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">

    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <meta name="description" content="Welcome to Star Hospital">

    <meta name="keywords" content="Nepal,Star Hospital,kathmandu">

    <title>Welcome to Star Hospital</title>

    <meta property="og:locale" content="en_US" />

    <meta property="og:type" content="website" />

    <meta itemprop="name" content="" />
    <meta itemprop="description" content="" />

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Bootstrap core CSS -->
 <link href="{{ asset('/css/bootstrap.min.css') }}" rel="stylesheet"> 
 <script src="{{asset ('/js/jquery.min.js') }}"></script>
 <link rel="stylesheet" href="{{ asset('/css/owl.carousel.min.css') }}">
 <link rel="stylesheet" href="{{ asset('/css/owl.theme.default.min.css') }}">
 <link href="{{ asset('/css/style.css') }}" rel="stylesheet" />
 <link href="{{ asset('/css/custom.min.css') }}" rel="stylesheet" />
 <link href="https://fonts.googleapis.com/css2?family=Raleway&display=swap" rel="stylesheet">
 <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300;400&display=swap" rel="stylesheet">

 <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>


<script type="text/javascript" src="{{asset ('/js/turn.min.js') }}"></script>
<script type="text/javascript" src="{{asset ('/js/turn.js') }}"></script>
 <style>
 body {
    padding-top: 54px;
}

.required {
    color: red;
}

@media (min-width: 992px) {
    body {
        padding-top: 56px;
    }
}

@media (min-width: 576px) {
    .modal-dialog {
        max-width: 700px !important;
    }
}

.toast-top-center {
    top: 500px;
    left: 10px;
}

.field-validation-error {
    color: red;
    font-size: 12px;
}

.textAreaMessage {
    height: 134px !important;
}
</style>
<link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
<link href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/themes/base/jquery-ui.css" rel="stylesheet" type="text/css" />
<style>
.carousel-indicators li {
    border-radius: 12px;
    width: 12px;
    height: 12px;
    background-color: #0392ce;
}
.carousel-indicators {
    margin-right: 60%;
    margin-left: 25%;
}
.carousel-indicators .active {
    background-color: #8c2888;
}
</style>

</head>


<body>
    @include('layouts.header')
    <section class="floating-menu">
        <div class="show-list">
            <a href="javascript:;" data-toggle="modal" data-backdrop="static" data-keyboard="false" data-target="#FeedbackModal">
            <i class="fa fa-edit">
            </i>
            <span>Suggest Us</span>
        </a>
    </div>
    </section>
    @yield('content')
    @include('layouts.footer')
    <div class="modal fade" id="FeedbackModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <section class="contact-form">
                    <div class="container">
                        <div class="col-md-12 mt-3 bg-white  shadow-box  md-3">
                   
                            <div class="row">
                                <div class="col-md-12 bg-white pd-3">
                                    <form action="/sendemail/feedback" method="post" id="FormCreate">
                                    {{ csrf_field() }}
                                       <div class="row mt-3">
                                           <div class="col-md-12">
                                            <h5>
                                                Provide Us your Valuable Suggestions
                                               
                                            </h5>
                                            <hr>
                                          </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Full Name <span class="required">*</span></label>
                                            <input Placeholder="Full Name" class="form-control" id="FullName" name="FullName" required="required" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Mobile <span class="required">*</span></label>
                                            <input Placeholder="Phone" class="form-control" id="Phone" name="Phone" required="required" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">Email</label>
                                            <input Placeholder="example@email.com" class="form-control" id="Email" name="Email" type="text" value="" />
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <label for="exampleInputEmail1">What are you looking for? <span class="required">*</span></label>
                                            <select class="form-control" id="MessageType" name="MessageType" required="required">
                                            <option value="Suggestion">Suggestion</option>    
                                            <option value="Complaint">Complaint</option>
    
                                            </select>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-12">
                                            <br>
                                            <label for="exampleInputEmail1">Message <span class="required">*</span></label>
                                            <textarea class="form-control" id="Message" name="Message" required="required" rows="10" style="height:100px;">
                                            </textarea>
                                            <div class="help-block with-errors"></div>
                                        </div>
                                        <div class="col-md-6">
                                            <button type="button" class="btn btn-danger btn-100" data-dismiss="modal" style="width:84px;">Close</button>
                                            <button type="submit" class="btn btn-custom btn-100" style="width:84px;border-radius: 2px;">
                                                Send
                                            </button>


                                        </div>
                                    </div>
                                 </form>                                
                               </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <div class="modal fade" id="messageAlert" tabindex="-1" role="dialog" aria-labelledby="messageAlertTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-body" id="essageAlertModalBody">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

</body>

<script>
$(function () {
    $('ul.nav li').on('click', function () {
        $(this).parent().find('li.active').removeClass('active');
        $(this).addClass('active');
    });

    $('#item-carouselCOE').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        nav: false,
        dots: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });
    $('#item-carouselHP').owlCarousel({
        loop: true,
        margin: 10,
        autoplay: true,
        nav: true,
        dots: true,
        lazyLoad: false,
        freeDrag: false,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 2
            },
            1000: {
                items: 4
            }
        }
    });
    $('#item-carousel').owlCarousel({
        loop: false,
        margin: 10,
        nav: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    })
    $('.carouselSlide').carousel({
        interval: 4000
    })

    var owl = $('.owl-carousel');
      
owl.on('drag.owl.carousel', function (event) {
    $('body').css('overflow', 'hidden');
});


});
</script>


<script src="{{ asset('/js/bootstrap.bundle.min.js') }}"></script>
<script src="{{ asset('/js/jquery.flexslider-min.js') }}" type="text/javascript"></script>
<script src="{{ asset('/js/jquery.easing.sliderkitpack.js') }}"></script>
<script src="{{ asset('/js/jquery.flexslider.js') }}"></script>
 <script src="{{ asset('/js/owl.carousel.min.js') }}"></script>

    
 <script type="text/javascript">
  jQuery(window).load(function() {
    jQuery('#flexslider-93').flexslider({
        animation: "slide",
        easing:"linear",                // I disable this option because there was a bug with Jquery easing and Joomla 3.2.3
    direction: "horizontal",        //String: Select the sliding direction, "horizontal" or "vertical"
    slideshowSpeed: 3500,       // How long each slide will show
    animationSpeed: 2000,       // Slide transition speed
      directionNav: true,             
      controlNav: false,    
      pauseOnHover: true,
        itemWidth:120,
          initDelay: 0,
          randomize: false,
      smoothHeight: false,
          minItems: 1,
          maxItems: 6,
          move: 6,
          touch: false,
      keyboardNav: true
      
    });
  });
</script>

<script>
$(document).ready(function () {
    var owl = $('.owl-carousel');
    owl.owlCarousel({
        loop: true,
        nav: true,
        margin: 10,
        autoplay: true,
        autoplayTimeout: 2500,
        autoplayHoverPause: true,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 4
            },
            960: {
                items: 4
            },
            1200: {
                items: 4
            }
        }
    });
    owl.on('mousewheel', '.owl-stage', function (e) {
        if (e.deltaY > 0) {
            owl.trigger('next.owl');
        } else {
            owl.trigger('prev.owl');
        }
        e.preventDefault();
    });
            // Floating Menu
            $(".floating-menu .show-list").hover(function () {
                $(this).toggleClass("active");
            });
        })
        
        </script>
        <script type="text/javascript">
        $(document).ready(function () {
            //carousel options
            $('#quote-carousel').carousel({
                pause: true, interval: 10000,
            });
    });

function feedbackFormValidation() {
    var FullName = $("#FullName").val();
    var Email = $("#Email").val();
    var Phone = $("#Phone").val();
    var MessageType = $("#MessageType").val();
    var Message = $("#Message").val();
    if (!FullName) {
        $("#msgErrorFullName").html('Full Name Required');
        return false;
    }
    else {
        $("#msgErrorFullName").html('');
    }
    if (Email) {
        if (!isValidEmail(Email)) {
            $("#msgErrorEmail").html('Valid Email Required');
            $("#Email").focus();
            return false;
        }
        else {
            $("#msgErrorEmail").html('');
        }
    }
    else {
        $("#msgErrorEmail").html('');
    }
    if (!Phone) {
        $("#msgErrorPhone").html('Phone Required');
        return false;
    }
    else {
        if (!validatePhone(Phone)) {
            $("#msgErrorPhone").html('Valid Mobile Required');
            $("#Phone").focus();
            return false;
        }
        else {
            $("#msgErrorPhone").html('');
        }

    }
    if (!MessageType) {
        $("#msgErrorMessageType").html('Message Type Required');
        return false;
    }
    else {
        $("#msgErrorMessageType").html('');
    }
    if (!Message) {
        $("#msgErrorMessage").html('Message Required');
        return false;
    }
    else {
        $("#msgErrorMessage").html('');
    }
    return true;
}
function isValidEmail(email) {
    return /^[a-z0-9]+([-._][a-z0-9]+)*@([a-z0-9]+(-[a-z0-9]+)*\.)+[a-z]{2,4}$/.test(email)
    && /^(?=.{1,64}@.{4,64}$)(?=.{6,100}$).*/.test(email);
}
function validatePhone(Mobile) {
    var filter = /^\d{10}$/;
    if (filter.test(Mobile)) {
        return true;
    }
    else {
        return false;
    }
}
function DisplayAlertMessage(body) {
    $("#essageAlertModalBody").html(body);
    $('#messageAlert').modal({
        backdrop: 'static',
        keyboard: false
    })
    $("#frmFeedback")[0].reset();
    $('#FeedbackModal').modal('hide');
    $("#feedbackLoader").hide();
    $("#btnFeedbackSave").show();
    setTimeout(function () {
        $("#messageAlert").modal('hide');
    }, 4000);
}
</script>

<script>
window.dataLayer = window.dataLayer || [];
function gtag() { dataLayer.push(arguments); }
gtag('js', new Date());

gtag('config', 'UA-114108040-1');
</script>
<script type="text/javascript">
function SaveNewsLetterDetails() {
    var k = jQuery.noConflict();
    var value = k('#EmailId').val();
    if (isEmail(value)) {
        k.ajax({
            url: "/Home/InsertNewsletterDetails",
            data: { EmailId: value },
            datatype: 'json',
            type: 'POST',
            success: function (data) {
                if (data) {

                    alert(data);
                    k("#EmailId").val("");

                }
            },
            error: function () {
                alert('Failed.Please Try again.');
            }
        });

    }
    else {
        alert('Invalid Email Address.');
    }


};

function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
</script>

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8/jquery-ui.min.js"></script>
<script type="text/javascript">
$(document).ready(function () {
    loadAutoComplete();
});
function loadAutoComplete() {
    $("#txtMasterSearch").autocomplete({
        select: function (event, ui) {
            if (ui.item.url.length <= 0) {
                return false;
            }
            if (ui.item.contentType.length <= 0) {
                return false;
            }
            if (ui.item.slug.length <= 0) {
                return false;
            }
            var url = ui.item.url + "/" + ui.item.contentType + '/' + ui.item.slug;
            window.location.href = url;
        },
        source: function (request, response) {
            $("#imgLoader").show();
            $.ajax({
                url: "/Home/GetMasterSearch",
                data: { term: request.term },
                dataType: "json",
                success: function (data) {
                    $("#imgLoader").hide();
                    response($.map(data, function (item) {
                        return item;
                    }))
                },
            });
        }
    })
}

</script>
<script type="text/javascript">
    $(window).load(function () {
    $('#NoticeModal').modal('show');
 });
</script>
</html>
