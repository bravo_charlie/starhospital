-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Mar 12, 2020 at 10:30 AM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.3.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `hospitalstar`
--

-- --------------------------------------------------------

--
-- Table structure for table `book_appointments`
--

CREATE TABLE `book_appointments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `DoctorDepartmentname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Doctorname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `appointmentdate` date NOT NULL,
  `reason` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `nation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gender` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `book_appointments`
--

INSERT INTO `book_appointments` (`id`, `DoctorDepartmentname`, `Doctorname`, `appointmentdate`, `reason`, `name`, `email`, `address`, `phone`, `age`, `nation`, `Gender`, `created_at`, `updated_at`) VALUES
(1, 'General Surgery, Minimally Invasive Surgery and Oncosurgery', 'Dr. Dinesh Shrestha', '2020-03-19', 'headache,fever', 'ashish', 'info@nepgeeks.com', 'kathmandu', '9949182781', 23, 'Nepal', 'Male', '2020-03-05 01:57:54', '2020-03-05 01:57:54');

-- --------------------------------------------------------

--
-- Table structure for table `departments`
--

CREATE TABLE `departments` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `departmentname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `departments`
--

INSERT INTO `departments` (`id`, `departmentname`, `created_at`, `updated_at`) VALUES
(1, 'Pulmonology', '2020-03-03 03:52:15', '2020-03-03 04:02:22'),
(2, 'Gastroenterology', '2020-03-03 04:01:09', '2020-03-03 04:02:37'),
(3, 'Neuro & Spine Surgery', '2020-03-03 04:01:19', '2020-03-03 04:02:50'),
(4, 'General Practice & Emergency Medicine', '2020-03-03 04:03:02', '2020-03-03 04:03:02'),
(5, 'General Surgery, Minimally Invasive Surgery and Oncosurgery', '2020-03-04 00:20:26', '2020-03-04 00:20:26'),
(6, 'Gastroenterology', '2020-03-08 03:22:46', '2020-03-08 03:22:46'),
(7, 'General Practice & Emergency Medicine', '2020-03-08 03:23:03', '2020-03-08 03:23:03'),
(8, 'Pathology', '2020-03-08 04:29:36', '2020-03-08 04:29:36'),
(9, 'Pulmonology', '2020-03-08 04:29:41', '2020-03-08 04:29:41'),
(10, 'Radiology', '2020-03-08 04:29:47', '2020-03-08 04:29:47');

-- --------------------------------------------------------

--
-- Table structure for table `doctors`
--

CREATE TABLE `doctors` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `Dept_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `education` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `speciality` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `opdtime` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `doctors`
--

INSERT INTO `doctors` (`id`, `name`, `Dept_id`, `image`, `education`, `speciality`, `opdtime`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Dr. Shail Rupakheti', '5', 'doctor1583989252.jpg', 'MBBS (MCOMS), MS (BPKIHS) Fellowship in GI Surgery(USA)', '<p><strong>Consultant GI, Laparoscopy &amp; HPB Surgeon</strong></p>', '11 am to 6 pm (Sunday to Friday)', '<p>test in phase</p>', '2020-03-04 00:22:42', '2020-03-11 23:16:36'),
(2, 'Dr. Dinesh Shrestha', '5', 'doctor1583383439.jpg', 'MBBS , MD', '<p>Sr. Consultant Gastroenterologist</p>', '1:00pm  to  5:00 pm', '<p>test</p>', '2020-03-04 22:58:59', '2020-03-04 22:58:59');

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `event_name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `event_description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `image`, `event_description`, `created_at`, `updated_at`) VALUES
(1, 'Holiday for Chaitra Dashain on 23rd march', 'events1583988073.jpg', '<p>Rama Navami is a spring Hindu festival that celebrates the birthday of lord Rama. He is particularly important to the Vaishnavism tradition of Hinduism, as the seventh avatar of Vishnu.[3][4] The festival celebrates the descent of god Vishnu as Rama avatar, through his birth to King Dasharatha and Queen Kausalya in Ayodhya.[5] The festival is a part of the spring Navratri, and falls on the ninth day of the bright half (Shukla Paksha) in the Hindu calendar month of Chaitra. This typically occurs in the Gregorian months of March or April every year.[6] Rama Navami is an optional government holiday in India.[7]</p>', '2020-03-11 22:56:13', '2020-03-12 00:12:26'),
(2, 'Holiday on ram Navami', 'events1583989474.jpg', '<p>Rama Navami is a spring Hindu festival that celebrates the birthday of lord Rama. He is particularly important to the Vaishnavism tradition of Hinduism, as the seventh avatar of Vishnu.[3][4] The festival celebrates the descent of god Vishnu as Rama avatar, through his birth to King Dasharatha and Queen Kausalya in Ayodhya.[5] The festival is a part of the spring Navratri, and falls on the ninth day of the bright half (Shukla Paksha) in the Hindu calendar month of Chaitra. This typically occurs in the Gregorian months of March or April every year.[6] Rama Navami is an optional government holiday in India.[7]</p>', '2020-03-11 23:19:34', '2020-03-12 00:12:49'),
(3, 'holiday on new year', 'events1583991982.jpg', '<p>welcome resort</p>', '2020-03-12 00:01:22', '2020-03-12 00:01:22');

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `name`, `image`, `created_at`, `updated_at`) VALUES
(1, 'Infrastructure', 'gallery1583645945.jpg', '2020-03-07 23:54:05', '2020-03-08 00:18:37'),
(2, 'Team', 'gallery1583647097.jpg', '2020-03-08 00:13:17', '2020-03-08 00:13:17'),
(3, 'Technology', 'gallery1583647391.jpg', '2020-03-08 00:18:11', '2020-03-08 00:18:11'),
(4, 'Activities', 'gallery1583647589.jpg', '2020-03-08 00:21:29', '2020-03-08 00:21:29'),
(5, 'Patient Experience', 'gallery1583647649.jpg', '2020-03-08 00:22:29', '2020-03-08 00:22:29'),
(6, 'Special Visits', 'gallery1583647709.jpg', '2020-03-08 00:23:29', '2020-03-08 00:23:29');

-- --------------------------------------------------------

--
-- Table structure for table `gallery_details`
--

CREATE TABLE `gallery_details` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `Gal_id` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `gallery_details`
--

INSERT INTO `gallery_details` (`id`, `Gal_id`, `image`, `created_at`, `updated_at`) VALUES
(5, '2', 'C:\\xampp\\tmp\\php3EC2.tmp', '2020-03-10 00:31:16', '2020-03-10 00:31:17'),
(6, '1', 'C:\\xampp\\tmp\\phpC09.tmp', '2020-03-10 00:32:09', '2020-03-10 00:32:09'),
(7, '6', 'C:\\xampp\\tmp\\php7F25.tmp', '2020-03-10 00:33:44', '2020-03-10 00:33:44'),
(8, '3', 'C:\\xampp\\tmp\\php707E.tmp', '2020-03-10 00:41:19', '2020-03-10 00:41:19'),
(9, '3', 'C:\\xampp\\tmp\\php6BEB.tmp', '2020-03-10 00:42:23', '2020-03-10 00:42:23');

-- --------------------------------------------------------

--
-- Table structure for table `international_parties`
--

CREATE TABLE `international_parties` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `fullname` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` int(11) NOT NULL,
  `nation` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `Gender` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `passportnumber` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `international_parties`
--

INSERT INTO `international_parties` (`id`, `fullname`, `email`, `address`, `phone`, `age`, `nation`, `Gender`, `passportnumber`, `created_at`, `updated_at`) VALUES
(1, 'ashish', 'info@nepgeeks.com', 'kathmandu', '9949182781', 25, '2', 'Female', '0748884054', '2020-03-05 03:55:28', '2020-03-05 03:55:28'),
(2, 'radhika budhathoki', 'budhathokiradhika886@gmail.com', 'changu_4bkt, changu_4bkt', '9860147496', 25, 'Nepal', 'Female', '3546288935', '2020-03-05 04:26:28', '2020-03-05 04:26:28');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2020_03_02_051407_create_view_counts_table', 1),
(5, '2020_03_02_064543_create_services_table', 2),
(6, '2020_03_03_055716_create_news_table', 3),
(7, '2020_03_03_091856_create_departments_table', 4),
(8, '2020_03_03_095522_create_doctors_table', 5),
(10, '2020_03_05_072634_create_book_appointments_table', 6),
(11, '2020_03_05_074553_create_send_mails_table', 7),
(15, '2020_03_05_083824_create_international_parties_table', 8),
(16, '2020_03_06_100857_create_testimonials_table', 9),
(17, '2020_03_08_051942_create_galleries_table', 10),
(18, '2020_03_08_063352_create_gallery_details_table', 11),
(19, '2020_03_12_042431_create_events_table', 12);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `newstitle` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `newstitle`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, '12TH ROTARY DISTRICT CONFERENCE 2019/2020', 'news1583216104.jpg', '<p>Our team of executives and officers representing STAR HOSPITAL at 12th Rotary District Conference 2019/2020 Pokhara.</p>', '2020-03-03 00:30:04', '2020-03-03 00:36:04'),
(2, '‘नेपाल प्लाष्टिक सर्जरीमा मेडिकल हब बन्न सक्छ’ डा. जसवन शाक्य', 'news1583216526.jpg', '<p>अहिले पनि धेरै मानिस प्लाष्टिक सर्जरीमा प्लाष्टिकको प्रयोग हुन्छ भन्ने ठान्छन्। प्लाष्टिक सर्जरी के हो, किन गरिन्छ र यसकाेअवस्था कस्ताे छ लगायत सेरोफेरोमा रहेर सानेपास्थित स्टार अस्पतालमा कार्यरत वरिष्ठ प्लाष्टिक सर्जन डा. जसवन शाक्यसँग हेल्थ टिभी अनलाइनले गरेको कुराकानी&ndash;</p>\r\n\r\n<p>प्लाष्टिक सर्जरी भनेको के हो?</p>\r\n\r\n<p>धेरैले मलाई सोध्छन्, &lsquo;प्लाष्टिक सर्जरीमा प्लाष्टिक प्रयोग हुने हो?&rsquo; प्लाष्टिक ग्रीक शब्द हो। यसको अर्थ हुन्छ मिलाउनु। प्लाष्टिक सर्जरीमा प्लाष्टिकको प्रयोग हुने होइन। जसरी प्लाष्टिकलाई मोडेर विभिन्न आकार प्रकार दिइन्छ, त्यसरी नै सेवाग्राहीको आवश्यकताअनुसार छालालाई विभिन्न आकार दिनु नै प्लाष्टिक सर्जरी हो। प्लाष्टिक सर्जरी दुई प्रकारको हुन्छ, रिकन्स्ट्रक्टिभ र कस्मेटिक।</p>\r\n\r\n<p>यी दुईमा के फरक छ त?</p>\r\n\r\n<p>रिकन्स्ट्रक्टिभ सर्जरी र कस्मेटिक सर्जरीमा धेरै नै फरक छ। रिकन्स्ट्रक्टिभ सर्जरी बाध्यता हो भने कस्मेटिक रहरमा गरिन्छ। जस्तो आगोले पोलेमा, दुर्घटनामा अंगभंग भएमा वा जन्मजात कुनै अंग निमलेमा ती अंग प्लाष्टिक सर्जरी गरेर आकारमा ल्याइन्छ। यस्तो अवस्थामा बिरामीको शरीरको कुनै अंगबाट छाला लिएर बिग्रिएको अंगमा राखिन्छ। तर ओठ, नाक, आँखा, स्तनफ कान आदि आफ्नो अंगको आकार चित्त नबुझ्दा वा अझ आकर्षक बनाउन कस्मेटिक सर्जरी गर्न सकिन्छ।</p>\r\n\r\n<p>कस्मेटिक सर्जरीको कुरा गर्दा नेपालमा कुन अंगको बढी हुन्छ?</p>\r\n\r\n<p>मेरो १० वर्षको अनुभवमा चिम्से आँखा ठूलो पार्ने, स्तन ठूलो बनाउने तथा टाइट बनाउने नाक मिलाउने, पेटको बोसो तान्ने बढी छन्। स्वभावैले यो सुन्दरतासँग जोडिने हुनाले प्लाष्टिक सर्जरी गराउनेमा पुरुषको तुलनामा महिलाहरू नै बढी हुन्छन्। यीमध्ये आँखाको &lsquo;लिड&rsquo; बनाउने भनेर आउनेको संख्या अझ बढी छ। आँखामा गाजल लगाउन पाइएन भन्दै धेरै जना आउँछन्। कस्मेटिक सर्जरीले राम्रो बनाउने मात्र नभई मानिसको आत्मविश्वास पनि बढाउँछ।</p>\r\n\r\n<p>सबैले गर्न मिल्छ त प्लाष्टिक सर्जरी?</p>\r\n\r\n<p>रिकन्स्ट्रक्टिभ सर्जरी उपचारकै एउटा पार्ट भएकाले यो जुन उमेर समूहलाई पनि गर्न सकिन्छ। कस्मेटिक सर्जरीमा भने उमेर हद हुन्छ। १८ वर्ष नपुगेकालाई हामी कस्मेटिक सर्जरी गर्दैनौँ। किनकी १८ वर्षसम्म उनीहरूको बढ्ने उमेर नै हुन्छ। अंगको विकास भइरहेको हुन्छ। एकपटक मिलाइसकेपछि फेरि बढ्यो भने समस्या हुन्छ। १८ वर्षपछि भने जुनसुकै उमेरकाले गराउन सक्छन्।</p>\r\n\r\n<p>नेपालमा कस्तो छ यसको सेवा एवं गुणस्तर?</p>\r\n\r\n<p>केही समय अघिसम्म सेलिब्रेटीहरूले मात्र कस्मेटिक सर्जरी गराएर राम्रो भएको धेरै सुनिन्थ्यो। तर अहिले सेलिब्रेटीले मात्र होइन सर्वसाधारणले पनि गराउन थालेका छन्। राम्रो हुन सबैलाई मन लाग्छ। नेपालमा पनि यो सेवा निकै फस्टाइरहेको छ। दक्ष जनशक्ति पनि बढिरहेको छ। निजी स्तरका ठूला अस्पतालमा पनि यो सेवा उपलब्ध छन्। हामीले स्टार अस्पतालमा पनि प्लाष्टिक सर्जरी सम्बन्धी सम्पूर्ण सेवा दिइरहेका छौँ।</p>\r\n\r\n<p>खर्चकाे कुरा गर्दा कतिकाे महँगाे छ?</p>\r\n\r\n<p>कस्मेटिक सर्जरी केही महँगो त हुन्छ नै। तर विदेशको तुलनामा यो असाध्यै सस्तो हो। यो सर्जरी विदेशतिर इन्स्युरेन्समा पर्दैन जसले गर्दा एकदमै महँगो हुन्छ। जसले गर्दा विदेशमा बस्ने धेरै नेपालीहरू नेपालमा आएर कस्मेटिक सर्जरी गराएर जान्छन्। विदेशमा बस्ने नेपालीहरू जसरी दाँतको उपचार गर्न नेपाल आउँछन त्यसैगरी कस्ममेटिक उपारका लागि पनि आउने गरेका छन्।</p>\r\n\r\n<p>नेपालले यो वर्ष भिजिट नेपाल मनाइरहेको छ। यो अवसरमा नेपाल प्लाष्टिक सर्जरीका लागि मेडिकल हब बन्न सक्छ। विदेशीलाई बोलाउने होइन, हाम्रै नेपाली, जो विदेशमा महँगो पैसा तिरेर कस्मेटिक सर्जरी गरिरहेका छन्, उनीहरुलाई यो सेवाबारे जानकारी दिएर ल्याउन सके धेरै नै राम्रो हुन्थ्यो। विदेशमा दिने सेवा भन्दा १ प्रतिशत पनि कम छैन यहाँको सेवा। साथै विदेशमा खर्च गर्ने पैसामा कस्मेटिक सर्जरी गर्ने मात्र होइन, परिवार र आफन्त भेटेर नेपाल घुमेर जान सकिन्छ।</p>\r\n\r\n<p>कस्मेटिक सर्जरी कत्तिको सुरक्षित मानिन्छ?</p>\r\n\r\n<p>प्लाष्टिक सर्जरीमध्ये रिकन्स्ट्रक्टिभ सर्जरीमा खासै रिस्क छैन। तर कस्मेटिक सर्जरीमा भने रिस्क हुनसक्छ। किनकी यो नमिलेको अंगलाई मिलाउने कुरा हो। यसमा सेवाग्राहीले १०० प्रतिशको आशा गरिरहेको हुन्छ। कहिलेकाहीँ सोचेजस्तो नहुन सक्छ। त्यसैले कस्मेटिक सर्जरी गराउँन राम्ररी बुझेर जानुपर्छ। दक्ष एवं अनुभवी डाक्टरकोमा जानुपर्छ। साथै यसको प्रक्रिया र साइड इफेक्ट सबै बुझ्न जरुरी छ।</p>\r\n\r\n<p>बुझेर दक्ष सर्जनबाट गराउँदा यो सुरक्षित नै हुन्छ। तर जहाँ पायो त्यहाँ गराउनु हुँदैन। कस्मेटिक सर्जरी दैनिक रूपमा गरिने मेकअप जस्तो होइन। एकपटक गराएपछि सधैँका लागि हाे।</p>', '2020-03-03 00:37:06', '2020-03-03 00:37:06'),
(3, 'PAKISTAN AMBASSADOR TO NEPAL VISITED STAR HOSPITAL', 'news1583217471.jpg', '<p>It was our immense pleasure to receive and give a tour of Star Hospital to His Excellency Dr. Mazhar Javed, Ambassador of the Embassy of the Islamic Republic of Pakistan in Nepal, along with Mr. Adnan Javed Khan, the Counsellor. We are honored by His Excellency&rsquo;s visit and are appreciative of his interest in the improvement of healthcare sector in the country. His Excellency, himself being a medical doctor, had some valuable inputs and suggestions for us. Star Hospital family extends gratitude for this special visit by the Ambassador and Counsellor. #starhospital #compassionatecarewithcomfort</p>', '2020-03-03 00:52:51', '2020-03-03 00:52:51'),
(4, 'STAR HOSPITAL LIMITED LAUNCHES “STAR ASSURANCE” HEALTH INSURANCE POLICY', 'news1583217744.jpg', '<p><strong>STAR HOSPITAL LIMITED</strong>&nbsp;launches the first and one of its kind health insurance policy in the country, which gives 100% cashless service that is reliable, hassle free and instant.This program was inaugurated by the Chairman of the National Assembly of Nepal, and was graced by the august presence of State Minister of Health and Population, Mayor of Lalitpur as well as many other dignitaries. This product is encouraged and inspired by the govt drive to insure people who cannot afford to pay immediately, and/or upfront during medical issues. This is a joint effort by Star Hospital Limited, Shikhar Insurance, Citizens Life Insurance and KFA Consulting Co.</p>\r\n\r\n<p>StarAssurance provides cashless service in over 5000 hospitals in India too. Isn&rsquo;t that assurance enough? Get yourself insured TODAY!<br />\r\n#STARASSURANCE #STARCARE #STARservices #STARHOSPITAL</p>\r\n\r\n<p>For more information please get in touch with our StarAssurance officers at the lobby.</p>', '2020-03-03 00:57:24', '2020-03-03 00:57:24');

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `send_mails`
--

CREATE TABLE `send_mails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `servicename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `serviceimage` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `servicedescription` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `servicename`, `serviceimage`, `servicedescription`, `created_at`, `updated_at`) VALUES
(1, '24 Hour Radiology', 'events1583146215.jpg', '<p><strong>Star Hospital Services</strong></p>\r\n\r\n<p>&nbsp;</p>\r\n\r\n<p>Star Hospital Care Credit caters to the financial need of the customers by providing easy financial scheme on health care requirement. It is hassle-free and trustworthy credit facility llowing customers to pay for their medical requirements via Credit Card. Nepalese with regular sources of income such as salary, rental and or business income are eligible to obtain Care Credit facility. Customer can make easy repayment via Credit Card. To inquire and apply for this facility, customers can visit our dedicated counter at Star Hospital or nearest MBL Branches as per convenience.</p>\r\n\r\n<p><strong>Features</strong></p>\r\n\r\n<ul>\r\n	<li>Maximum loan amount NPR 700,000.00</li>\r\n	<li>Loan Tenure: Maximum 24 months</li>\r\n	<li>Repayment Method: EMI</li>\r\n</ul>\r\n\r\n<p><strong>Process</strong></p>\r\n\r\n<ul>\r\n	<li>Submit a duly filled form at Credit Care Helpdesk at Star Hospital along with Citizenship, Passport size Photograph along with income statement for last 3 months.</li>\r\n	<li>Have the loan approved within 2 working days.</li>\r\n</ul>\r\n\r\n<p>&nbsp;</p>', '2020-03-02 01:27:41', '2020-03-02 23:13:08'),
(3, '24 Hour Pharmacy', 'events1583146183.jpg', '<p>Star Hospital features 24 hours pharmacy services. Our pharmacy has wide range of medicines.</p>', '2020-03-02 05:04:43', '2020-03-02 23:13:20'),
(4, '24 Hour Pathology', 'events1583146262.jpg', '<p>Star Hospital features 24 hours pathology services. To provide accurate reports, our pathology lab is fully equipped with equipments such as:</p>\r\n\r\n<ul>\r\n	<li>Fully-automated analyzers</li>\r\n	<li>ELISA Reader</li>\r\n	<li>Blood Gas Analyzer (ABG)</li>\r\n	<li>Latest microscopes, and all other necessary equipments.</li>\r\n</ul>', '2020-03-02 05:06:02', '2020-03-02 23:13:38'),
(5, '24 Hour Emergency', 'events1583146353.jpg', '<p>Test</p>', '2020-03-02 05:07:33', '2020-03-02 23:13:50'),
(6, 'Intensive Care Unit', 'events1583146419.jpg', '<p>Various ICUs at Star:</p>\r\n\r\n<ul>\r\n	<li>Intensive Care Unit (ICU)</li>\r\n	<li>Neonatal Intensive Care Unit (NICU)</li>\r\n	<li>Pediatric Intensive Care Unit (PICU)</li>\r\n</ul>', '2020-03-02 05:08:39', '2020-03-02 05:08:39'),
(7, 'Counselling', 'events1583146468.jpg', '<p>One of the main drawbacks in the healthcare system of Nepal is the lack of proper and adequate counseling to the patients and their relatives. This has created a situation of number of conflicts between healthcare workers and the patient&rsquo;s parties. We believe that this can be prevented by proper and adequate counseling. So our aim is to provide proper and adequate counseling to our clients by giving them quality health services.</p>', '2020-03-02 05:09:28', '2020-03-02 05:09:28'),
(9, 'Specialist Service', 'events1583211448.jpg', '<p>Our 100-bedded hospital has 22 specialty centers namely:</p>\r\n\r\n<ul>\r\n	<li>General Medicine</li>\r\n	<li>Hematology</li>\r\n	<li>Endocrinology</li>\r\n	<li>Hepatology</li>\r\n	<li>Pulmonology</li>\r\n	<li>Rheumatology</li>\r\n	<li>Cardiology</li>\r\n	<li>General Surgery and Laparoscopic Surgery</li>\r\n	<li>Neurosurgery</li>\r\n	<li>Urosurgery</li>\r\n	<li>Gynecology and Obstetrics</li>\r\n	<li>Orthopedics and Ortho Plastic</li>\r\n	<li>Gastroenterology</li>\r\n	<li>Dermatology and Venereal Diseases</li>\r\n	<li>Pediatrics</li>\r\n	<li>Ophthalmology</li>\r\n	<li>Dentistry</li>\r\n	<li>Psychiatry</li>\r\n	<li>Pathology</li>\r\n	<li>Anesthesia</li>\r\n	<li>Radiology</li>\r\n	<li>Cardiovascular and Thoracic Surgery</li>\r\n	<li>Physiotherapy</li>\r\n	<li>Extracorporeal Shock Wave Lithotripsy (ESWL)</li>\r\n	<li>ENT (Ears, Nose and Throat)</li>\r\n</ul>', '2020-03-02 23:12:28', '2020-03-02 23:12:28');

-- --------------------------------------------------------

--
-- Table structure for table `testimonials`
--

CREATE TABLE `testimonials` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `testimonials`
--

INSERT INTO `testimonials` (`id`, `name`, `image`, `description`, `created_at`, `updated_at`) VALUES
(1, 'Sandesh Shrestha hi', 'testimonial1583491893.jpg', '<p>A world-class quaternary care centre with Multiple Centers of Excellence, combining the finest medical minds in offering world class health care solutions. Nepal Mediciti combines the best of talents and technology to provide holistic treatment with a Multi-Disciplinary approach.</p>', '2020-03-06 04:45:10', '2020-03-11 23:17:36'),
(3, 'Sita Lamichhane', 'testimonial1583492354.jpg', '<p>Wonderful place for incredible care &amp;amp; comfort</p>', '2020-03-06 05:14:14', '2020-03-06 05:14:14');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Nepgeeks', 'admin@admin.com', NULL, '$2y$10$r49n.zBODGjx4Zg5q2p1FuWb3qGVvCEyoatw.LIzSk3fC/Sqgkaz2', NULL, '2020-03-01 23:32:12', '2020-03-01 23:32:12');

-- --------------------------------------------------------

--
-- Table structure for table `view_counts`
--

CREATE TABLE `view_counts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `ip` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `session_id` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `view_count` int(11) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `book_appointments`
--
ALTER TABLE `book_appointments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `departments`
--
ALTER TABLE `departments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `doctors`
--
ALTER TABLE `doctors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_details`
--
ALTER TABLE `gallery_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `international_parties`
--
ALTER TABLE `international_parties`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `send_mails`
--
ALTER TABLE `send_mails`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `testimonials`
--
ALTER TABLE `testimonials`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `view_counts`
--
ALTER TABLE `view_counts`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `book_appointments`
--
ALTER TABLE `book_appointments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `departments`
--
ALTER TABLE `departments`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `doctors`
--
ALTER TABLE `doctors`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `gallery_details`
--
ALTER TABLE `gallery_details`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `international_parties`
--
ALTER TABLE `international_parties`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `send_mails`
--
ALTER TABLE `send_mails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `testimonials`
--
ALTER TABLE `testimonials`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `view_counts`
--
ALTER TABLE `view_counts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
