<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInternationalPartiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('international_parties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('fullname');
            $table->text('email');
            $table->text('address');
            $table->text('phone');
            $table->integer('age');
            $table->text('nation');
            $table->text('Gender');
            $table->text('passportnumber');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('international_parties');
    }
}
