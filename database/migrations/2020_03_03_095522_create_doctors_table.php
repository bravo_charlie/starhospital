<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->text('dept_id');
            $table->text('image');
            $table->text('education')->nullable;
            $table->text('speciality')->nullable;
            $table->text('opdtime')->nullable;
            $table->text('biography')->nullable;
            $table->text('description')->nullable;
            $table->text('edudetails')->nullable;
            $table->text('language')->nullable;
            $table->text('membershipassociate')->nullable;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
