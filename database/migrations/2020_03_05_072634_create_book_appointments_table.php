<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBookAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('book_appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('DoctorDepartmentname');
            $table->string('Doctorname');
            $table->date('appointmentdate');
            $table->string('reason');
            $table->string('name');
            $table->text('email');
            $table->text('address');
            $table->text('phone');
            $table->integer('age');
            $table->text('nation');
            $table->text('Gender');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('book_appointments');
    }
}
