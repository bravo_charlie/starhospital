<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::fallback(function() {
    return view('nopage');
});

Route::get('/','HomePageController@index');
Route::get('/About','HomePageController@about');
Route::get('/magazine','HomePageController@magazine');
Route::get('/starwellness','HomePageController@starwell');
Route::get('/Contact','HomePageController@contact');
Route::get('/GetDirection','HomePageController@location');
Route::get('/gallery','HomePageController@gallery');
Route::get('/search','HomePageController@search');
Route::get('/qr-pay','HomePageController@qrpay');
Route::get('/bod','HomePageController@boarddirector');
Route::get('/feedback','HomePageController@feedback');
Route::post('/finddoctor','HomePageController@searchmethod');
Route::get('/finddoctor','HomePageController@searchonRefresh');
Route::get('/gallerydetails/{id}','HomePageController@galleryDetails');
Route::get('/pcr-test-request','HomePageController@pcrTestRequest');
Route::get('/sample-collection-form-for-for-suspected-covid-19','HomePageController@samplecollection');

Route::get('/eventdetails','HomePageController@event');
Route::get('/eventdetail/{id}','HomePageController@eventdetail');
Route::get('/eventdetailsAjax','HomePageController@eventAjax');

Route::get('/servicedetails/{id}','HomePageController@service');
Route::get('/servicedetailsAjax','HomePageController@serviceAjax');

Route::get('/News','HomePageController@news');
Route::get('/newsdetails/{id}','HomePageController@newsdetail');
Route::get('/newsdetailsAjax','HomePageController@newsAjax');

Route::get('/Doctor','HomePageController@doctor');
Route::get('/doctordetails/{id}','HomePageController@doctordetails');

Route::get('/Appointment','HomePageController@appointment');
Route::get('/international-patient','HomePageController@internationalparty');

Route::get('/testimonial','HomePageController@testimonialview');
Route::get('/readmore/{id}','HomePageController@testimonialdetails');

Route::get('/Chairman-Message','HomePageController@chairmanmsg');
Route::get('/Message-From-MD', 'HomePageController@messagemd');
Route::get('/centerforexcellence','HomePageController@centerexcellence');
Route::get('/Mission&Vision','HomePageController@missionandvision');

Auth::routes();
Route::get('/','HomePageController@index');

Route::get('/home', 'HomeController@index')->name('home');
Route::post('/sendemail/send', 'SendMailController@send');
Route::post('/sendemail/bookappointment', 'SendMailController@bookappointment');
Route::post('/sendemail/internationalparty', 'SendMailController@international');
Route::post('/sendemail/feedback','SendMailController@feedback');
Route::post('/sendemail/pcr-test-request','SendMailController@pcrtestrequest');

Route::get('/home/service', 'ServiceController@index');
Route::get('/home/service/create', 'ServiceController@create');
Route::post('/home/service/create', 'ServiceController@store');
Route::get('/home/service/edit/{id}', 'ServiceController@edit')->name('service.edit');
Route::post('/home/service/edit/{id}', 'ServiceController@update')->name('service.update');
Route::delete('/home/service/destroy/{id}', 'ServiceController@destroy')->name('service.delete');

Route::get('/home/news', 'NewsController@index');
Route::get('/home/news/create', 'NewsController@create');
Route::post('/home/news/create', 'NewsController@store');
Route::get('/home/news/edit/{id}', 'NewsController@edit')->name('news.edit');
Route::post('/home/news/edit/{id}', 'NewsController@update')->name('news.update');
Route::delete('/home/news/destroy/{id}', 'NewsController@destroy')->name('news.delete');

Route::get('/home/modal', 'PopupController@index');
Route::get('/home/modal/create', 'PopupController@create');
Route::post('/home/modal/create', 'PopupController@store');
Route::get('/home/modal/edit/{id}', 'PopupController@edit')->name('modal.edit');
Route::post('/home/modal/edit/{id}', 'PopupController@update')->name('modal.update');
Route::delete('/home/modal/destroy/{id}', 'PopupController@destroy')->name('modal.delete');

Route::get('/home/events', 'EventController@index');
Route::get('/home/events/create', 'EventController@create');
Route::post('/home/events/create', 'EventController@store');
Route::get('/home/events/edit/{id}', 'EventController@edit')->name('event.edit');
Route::post('/home/events/edit/{id}', 'EventController@update')->name('event.update');
Route::delete('/home/events/destroy/{id}', 'EventController@destroy')->name('event.delete');

Route::get('/home/department', 'DepartmentController@index');
Route::get('/home/department/create', 'DepartmentController@create');
Route::post('/home/department/create', 'DepartmentController@store');
Route::get('/home/department/edit/{id}', 'DepartmentController@edit')->name('department.edit');
Route::post('/home/department/edit/{id}', 'DepartmentController@update')->name('department.update');
Route::delete('/home/department/destroy/{id}', 'DepartmentController@destroy')->name('department.delete');

Route::get('/home/doctor', 'DoctorController@index');
Route::get('/home/doctor/create', 'DoctorController@create');
Route::post('/home/doctor/create', 'DoctorController@store');
Route::get('/home/doctor/{id}', 'DoctorController@show');
Route::get('/home/doctor/edit/{id}', 'DoctorController@edit')->name('doctor.edit');
Route::post('/home/doctor/edit/{id}', 'DoctorController@update')->name('doctor.update');
Route::delete('/home/doctor/destroy/{id}', 'DoctorController@destroy')->name('doctor.delete');

Route::get('/home/testimonial', 'TestimonialController@index');
Route::get('/home/testimonial/create', 'TestimonialController@create');
Route::post('/home/testimonial/create', 'TestimonialController@store');
Route::get('/home/testimonial/edit/{id}', 'TestimonialController@edit')->name('testimonial.edit');
Route::post('/home/testimonial/edit/{id}', 'TestimonialController@update')->name('testimonial.update');
Route::delete('/home/testimonial/destroy/{id}', 'TestimonialController@destroy')->name('testimonial.delete');

Route::get('/home/gallery', 'GalleryController@index');
Route::get('/home/gallery/create', 'GalleryController@create');
Route::post('/home/gallery/create', 'GalleryController@store');
Route::get('/home/gallery/edit/{id}', 'GalleryController@edit')->name('gallery.edit');
Route::post('/home/gallery/edit/{id}', 'GalleryController@update')->name('gallery.update');
Route::delete('/home/gallery/destroy/{id}', 'GalleryController@destroy')->name('gallery.delete');

Route::get('/home/gallerydetails', 'GalleryDetailsController@index');
Route::get('/home/gallerydetails/create', 'GalleryDetailsController@create');
Route::post('/home/gallerydetails/create', 'GalleryDetailsController@store');
Route::delete('/home/gallerydetails/destroy/{id}', 'GalleryDetailsController@destroy')->name('gallerydetails.delete');

Route::get('/home/slider', 'SliderController@index');
Route::get('/home/slider/create', 'SliderController@create');
Route::post('/home/slider/create', 'SliderController@store');
Route::get('/home/slider/edit/{id}', 'SliderController@edit')->name('slider.edit');
Route::post('/home/slider/edit/{id}', 'SliderController@update')->name('slider.update');
Route::delete('/home/slider/destroy/{id}', 'SliderController@destroy')->name('slider.delete');