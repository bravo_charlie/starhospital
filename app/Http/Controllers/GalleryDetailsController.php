<?php

namespace App\Http\Controllers;

use App\GalleryDetails;
use App\Gallery;
use Illuminate\Http\Request;

class GalleryDetailsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $gallerydetails = GalleryDetails::all();
        $gallery = Gallery::all();
        return view('dashboard.gallerydetails.index', compact('gallery','gallerydetails'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       $gallery = Gallery::all();
       return view('dashboard.gallerydetails.create', compact('gallery'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $gallerydetails = new GalleryDetails();
        $gallerydetails->Gal_id = request('Gal_id');
        $imgname=[];
        if ($request->hasfile('images'))
        {
            foreach($request->file('images') as $image){
            $imageName = "gallerydetails".time().'.'. $image->getclientOriginalName();
            $location = public_path('/uploads/gallerydetails/');
            $image->move($location, $imageName);
            $imgname[]=$imageName;
           
           }
        }
          $gallerydetails->image=json_encode($imgname);
          $gallerydetails->save();
          return redirect('/home/gallerydetails');
    }
      /**
     * Display the specified resource.
     *
     * @param  \App\GalleryDetails  $galleryDetails
     * @return \Illuminate\Http\Response
     */
    public function show(GalleryDetails $galleryDetails)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GalleryDetails  $galleryDetails
     * @return \Illuminate\Http\Response
     */
    public function edit(GalleryDetails $galleryDetails)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GalleryDetails  $galleryDetails
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GalleryDetails $galleryDetails)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GalleryDetails  $galleryDetails
     * @return \Illuminate\Http\Response
     */
    public function destroy(GalleryDetails $galleryDetails,$id)
    {
         $gallerydetails = GalleryDetails::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
