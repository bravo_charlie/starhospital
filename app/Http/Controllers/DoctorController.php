<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use DB;
class DoctorController extends Controller
{
     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $doctors = Doctor::all();
        $departments = Department::all();
        return view('dashboard.doctor.index', compact('doctors','departments')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view ('dashboard.doctor.create', compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $doctors = new Doctor();
        $doctors->name = request('name');
        $doctors->dept_id = request('dept_id');
        $doctors->education = request('education');
        $doctors->speciality = request('speciality');
        $doctors->opdtime = request('opdtime');
        $doctors->biography = request('biography');
        $doctors->edudetails = request('edudetails');
        $doctors->language = request('language');
        $doctors->membershipassociate = request('membershipassociate');
        $doctors->description = request('description');
        $request->validate([
            'name' => ['required', 'min:4'],
            'education' => ['required', 'min:5'],
            'speciality' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'image' => 'image|mimes:jpg,png,jpeg|'
            
        ]);
         if(file_exists($request->file('image'))){
            $image = "doctor".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/doctor');
            $request->file('image')->move($location, $image);
            $doctors->image = $image;
        }
        else{
            $doctors->image = 'default-thumbnail.png';
        } 
        $doctors->save();
        return redirect('/home/doctor');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function show(Doctor $doctor)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function edit(Doctor $doctor,$id)
    {
        //echo("<script>console.log('PHP: " . $data . "');</script>");
        //echo("<script>console.log('i am in edit');</script>");
        $doctors = Doctor::findOrFail($id);
        $departments = Department::all();
        return view('dashboard.doctor.edit', compact('doctors','departments')); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Doctor $doctor,$id)
    { 
        //echo("<script>console.log('PHP: " . $data . "');</script>");
        // in case for doctor active inactive
        if (str_contains($id, '-')){
            $doctorID = Str::before($id, '-');   
            $flag = Str::after($id, '-') == '1' ? '0' : '1';
            DB::update('update doctors set is_listed = ? where id = ?',[$flag,$doctorID]);
            return redirect('/home/doctor');
        }

        $doctors = Doctor::findOrFail($id);
        $doctors->name = request('name');
        $doctors->Dept_id = request('dept_id');
        $doctors->education = request('education');
        $doctors->speciality = request('speciality');
        $doctors->opdtime = request('opdtime');
        $doctors->biography = request('biography');
        $doctors->edudetails = request('edudetails');
        $doctors->language = request('language');
        $doctors->membershipassociate = request('membershipassociate');
        $doctors->description = request('description');
        $request->validate([
            'name' => ['required', 'min:4'],
            'education' => ['required', 'min:5'],
            'speciality' => ['required', 'min:5'],
            'description' => ['required', 'min:10'],
            'image' => 'image|mimes:jpg,png,jpeg|'
            
        ]);

        if(file_exists($request->file('image'))){
            $image = "doctor".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/doctor');
            $request->file('image')->move($location, $image);
            $doctors->image = $image;
        }
        else{
            $doctors->image = $doctors->image;
        } 
        $doctors->save();
        return redirect('/home/doctor');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Doctor  $doctor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Doctor $doctor,$id)
    {
        $doctors = Doctor::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
