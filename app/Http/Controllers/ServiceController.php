<?php

namespace App\Http\Controllers;

use App\Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $services = Service::all();
        return view ('dashboard.service.index',compact('services'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       return view ('dashboard.service.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $services = new Service();
        $request->validate([
            'servicename' => 'required',
            'servicedescription' => 'required',
            'serviceimage' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->servicename = $request->servicename;
        $services->servicedescription = $request->servicedescription;
        if(file_exists($request->file('serviceimage'))){
            $image = "events".time().'.'.$request->file('serviceimage')->getclientOriginalExtension();
            $location = public_path('uploads/service');
            $request->file('serviceimage')->move($location, $image);
            $services->serviceimage = $image;
        }
        else{
            $services->serviceimage = 'default-thumbnail.png';
        }        
        $services->save();
        return redirect('/home/service');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function show(Service $service)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function edit(Service $service,$id)
    {
       $services = Service::findOrFail($id);
        return view ('dashboard.service.edit',compact('services'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Service $service,$id)
    {
       $services = Service::findOrFail($id);
        $request->validate([
            'servicename' => 'required',
            'servicedescription' => 'required',
            'serviceimage' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $services->servicename = $request->servicename;
        $services->servicedescription = $request->servicedescription;
        if(file_exists($request->file('serviceimage'))){
            $image = "events".time().'.'.$request->file('serviceimage')->getclientOriginalExtension();
            $location = public_path('uploads/service');
            $request->file('serviceimage')->move($location, $image);
            $services->serviceimage = $image;
        }
        else{
            $services->serviceimage = $services->serviceimage;
        }        
        $services->save();
        return redirect('/home/service');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Service  $service
     * @return \Illuminate\Http\Response
     */
    public function destroy(Service $service,$id)
    {
        $services = Service::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
