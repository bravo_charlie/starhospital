<?php

namespace App\Http\Controllers;

use App\Department;
use App\Doctor;
use Illuminate\Http\Request;

class DepartmentController extends Controller
{


     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $department = Department::paginate(9);
        return view ('dashboard.department.index',compact('department')); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.department.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $department = new Department();
        $request->validate([
            'departmentname' => 'required',
            
        ]);
        $department->departmentname = $request->departmentname;
        $department->save();
        return redirect('/home/department');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department,$id)
    {
        $department = Department::findOrFail($id);
        return view ('dashboard.department.edit',compact('department'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department,$id)
    {
       $department = Department::findOrFail($id);
        $request->validate([
            'departmentname' => 'required'
        ]);
        $department->departmentname = $request->departmentname;
        
        $department->save();
        return redirect('/home/department');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department,$id)
    {
        $department = Department::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
