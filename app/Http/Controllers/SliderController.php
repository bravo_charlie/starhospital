<?php

namespace App\Http\Controllers;

use App\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $sliders = Slider::all();
        return view ('dashboard.slider.index',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $sliders = new Slider();
        $request->validate([
           'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $sliders->name = $request->name;
        $sliders->link = $request->link;
        if(file_exists($request->file('image'))){
            $image = "slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/slider');
            $request->file('image')->move($location, $image);
            $sliders->image = $image;
        }
        else{
            $sliders->image = 'default-thumbnail.png';
        }        
        $sliders->save();
        return redirect('/home/slider');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function show(Slider $slider)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function edit(Slider $slider,$id)
    {
        $sliders = Slider::findOrFail($id);
        return view ('dashboard.slider.edit',compact('sliders'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Slider $slider,$id)
    {
       $sliders = Slider::findOrFail($id);
        $request->validate([
           'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $sliders->name = $request->name;
        $sliders->link = $request->link;
        if(file_exists($request->file('image'))){
            $image = "slider".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/slider');
            $request->file('image')->move($location, $image);
            $sliders->image = $image;
        }
        else{
           $sliders->image = $sliders->image;
        }        
        $sliders->save();
        return redirect('/home/slider');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Slider  $slider
     * @return \Illuminate\Http\Response
     */
    public function destroy(Slider $slider,$id)
    {
        $sliders = Slider::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
