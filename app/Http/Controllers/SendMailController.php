<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BookAppointment;
use App\SendMail;
use App\InternationalParty;
use App\Feedback;
use App\Pcrtest;
use App\Traits\sendSMS;

class SendMailController extends Controller
{
    function send(Request $request)
    {
    $this->validate($request, [
      'name'     =>  'required',
      'email'  =>  'required|email',
      'address' => 'required',
    
      'phone' =>  'required',
      'message' =>  'required'
     ]);

       $data = array(
            'name'      =>  $request->name,
            'subject'   =>  '',
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'address' =>  $request->address,
            'message'   =>   $request->message
        );

        $txt1 = '<html>
        <head>  
        </head>
        <body>
             <p>Hi, This is '. $data['name'] .'</p>
            <p>Email:'. $data['email'] .'<br><br>Phone No:'. $data['phone'] .'</p>
             <p>Address:' . $data['address'] . '</p>'.  '
               Message:<br>
             '. $data['message'] .'</p>
           
        </body>
        </html>';       

        $to = "covid@starhospitallimited.com";
        $subject = "Inquiry For Hospital";

        $headers = "From:starhospital.aeydentech.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        SendMail::create($request->all());
        $result=   mail($to,$subject,$txt1,$headers);
        return back()->with('success','Thanks for Visiting us!');
       }

 function bookappointment(Request $request){
  $this->validate($request, [
    'DoctorDepartmentname' => 'required',
    'Doctorname' => 'required',
    'appointmentdate' => 'required',
    'reason' =>  'required',
    'name'     =>  'required',
    'email'  =>  'required|email',
    'address' => 'required',
    'phone' =>  'required',
    'age' => 'required',
    'nation' => 'required',
    'Gender' => 'required'

     ]);

     $data = array(
      'DoctorDepartmentname' =>  $request->DoctorDepartmentname,
      'Doctorname'  =>  $request->Doctorname,
      'appointmentdate'  => $request->appointmentdate,
      'reason' =>  $request->reason,
      'name'      =>  $request->name,
      'email'   =>   $request->email,
      'address' =>  $request->address,
      'phone'   =>   $request->phone,
      'age'    =>   $request->age,
      'nation'   =>  $request->nation,
      'Gender' => $request->Gender
          );

       $txt2 = '<html>
             <head>  
             </head>
            <body>
                    <p>Hi, This is ' . $data['name'] . '</p>
                    <p>Email:' . $data['email'] .'<br><br>Phone Number:' . $data['phone'] . '</p>
                    <p>I have query for the '. $data['reason'] .' and I want service with ' . $data['Doctorname'] . ' in the date ' . $data['appointmentdate'] . '.</p>
                    <p>It would be appriciative, if i receive the appointment soon.</p>
            </body>
        </html>';       

        $to = "covid@starhospitallimited.com";
        $headers = "From:starhospital.aeydentech.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        BookAppointment::create($request->all());
        $result=   mail($to,$txt2,$headers);
        return back()->with('success','Your Appointment is Booked for the day.We will contact You soon.!');
      

 }

 function international(Request $request){
  $this->validate($request, [
      'fullname'     =>  'required',
      'email'  =>  'required|email',
      'address' => 'required',
      'phone' =>  'required',
      'age' => 'required',
      'nation' => 'required',
      'Gender' => 'required',
      'passportnumber' => 'required'
     ]);

  $data = array(
            'fullname'      =>  $request->fullname,
            'email'   =>   $request->email,
            'address' =>  $request->address,
            'phone'   =>   $request->phone,
            'age'    =>   $request->age,
            'nation'   =>  $request->nation,
            'Gender' =>  $request->Gender,
            'passportnumber' => $request->passportnumber
          );

   $txt3 = '<html>
             <head>  
             </head>
              <body>
                    <p> Hi, This is '. $data['fullname'] .'</p>
                    <p>Email:'. $data['email'] .'<br><br> Phone Number:'. $data['phone'] .'</p>
                    <p>I am from '. $data['nation'] . ' and my passport number is ' . $data['passportnumber'] . '</p>
                    <p>I would like to get information about the system for International Patient. </p>
        </body>
        </html>';       

        $to = "itdept@starhospitallimited.com";
        $subject = "Enquiry for International Patient Center (IPC)";

        $headers = "From:starhospital.aeydentech.com"."\r\n";
        $headers .= "Cc: anil.qode@gmail.com"."\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        InternationalParty::create($request->all());
        $result=   mail($to,$subject,$txt3,$headers);
        return back()->with('success','Thanks for visiting us');
 }

 function feedback(Request $request){

  $this->validate($request, [
      'FullName'     =>  'required',
      'Email'  =>  'required|email',
      'Phone' =>  'required',
      'MessageType' => 'required',
      'Message' => 'required'
    ]);

  $data = array(
            'FullName'      =>  $request->FullName,
            'Email'   =>   $request->Email,
            'Phone' =>  $request->Phone,
            'MessageType'   =>   $request->MessageType,
            'Message'    =>   $request->Message
          );
  $txt4 = '<html>
             <head>  
             </head>
              <body>
                    <p> <b>Patient Name: </b>'. $data['FullName'] .'</p>
                    <p><b>Email: </b>'. $data['Email'] .'<br><br> Phone Number:'. $data['Phone'] .'</p>
                     <p><b>Subject: </b>'. $data['MessageType'] . '<br><br>
               <b>Message: </b><br>
             '. $data['Message'] .'</p>
            </body>
        </html>';       

        $to = "	covid@starhospitallimited.com";
        $subject = "Feedback about the hospital services";
        $headers = "From: Star Hospital\r\n";
        $headers .= "Cc: anil.qode@gmail.com\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        Feedback::create($request->all());
        $result=   mail($to,$subject,$txt4,$headers);
        return back()->with('success','Thank you for your comment!!!');
    }

 function pcrtestrequest(Request $request){
  $this->validate($request, [
      'name'     =>  'required',
      'address' => 'required',
      'phone' =>  'required',
      'age'  => 'required',
      'sex' => 'required'
 
     ]);

       $data = array(
            'name'      =>  $request->name,
            'email'   =>   $request->email,
            'phone'   =>   $request->phone,
            'address' =>  $request->address,
            'age'  => $request->age,
            'sex' => $request->sex,
            'message'    =>   $request->message
        );

        $txt5 = '<html>
        <head>  
        </head>
        <body>
             <p><b>Name: </b>'. $data['name'] .'<br>
             <b>Email: </b>'. $data['email'] .'<br>
             <b>Phone No: </b>'. $data['phone'] .'<br>
             <b>Address: </b>' . $data['address'] . '<br>
             <b>Age: </b>' . $data['age'] . '<br>
             <b>Sex: </b>'. $data['sex'] . '<br>
             <b>Message: </b><br>
             '. $data['message'] .'</p>
       
        </body>
        </html>';       

        $to = "covid@starhospitallimited.com";
        $subject = "PCR TEST REQUEST";
        $headers = "From:covid@starhospitallimited.com" . "\r\n";
        $headers .= 'Cc: anil.qode@gmail.com' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";  
        $headers .= "Content-type: text/html; charset=ISO-8859-1\r\n";
        Pcrtest::create($request->all());
        $result=   mail($to,$subject,$txt5,$headers);
        $this->sendSms($request->name,$request->phone, $request->address);
        return back()->with('success','Your request has been sent.');
       }

   public function sendSms($name,$phone,$address)
    {


        $name = preg_replace('/\s+/', '%20', $name);
        $data = array(
            'name'=> $name,
            'Phone'=> $phone,
            'address'=> $address ,
         );
        $message =  $data['name'].'%20has%20requested%20for%20PCR%20Test%20from%20'.$data['address']. '%20%20%20Ph%20no:'.$phone;
       
        $this->sendMessage($message);
  } 

   public function sendMessage($message){

   // dd("https://api.sparrowsms.com/v2/sms/?from=InfoSMS&to=9818229023&token=ab93U6bPeVGbd7baUnoo&text=".$message);
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://api.sparrowsms.com/v2/sms/?from=InfoSMS&to=9801235917&token=ab93U6bPeVGbd7baUnoo&text=$message",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_HTTPHEADER => array(
            "Accept: */*",
            "Accept-Encoding: gzip, deflate",
            "Cache-Control: no-cache",
            "Connection: keep-alive",
            "Host: api.sparrowsms.com",
            "Postman-Token: c454b319-a6aa-4f5f-8a86-bd66ff99a219,efa8c637-d218-4410-889c-16614cd548b6",
            "User-Agent: PostmanRuntime/7.18.0",
            "cache-control: no-cache"
        ),
      ));

        $response = curl_exec($curl);
    
        $err = curl_error($curl);

        curl_close($curl);
    }
}