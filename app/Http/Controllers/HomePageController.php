<?php

namespace App\Http\Controllers;

use App\Service;
use App\News;
use App\Department;
use App\Doctor;
use App\Testimonial;
use App\Gallery;
use App\Event;
use App\GalleryDetails;
use App\Slider;
use Illuminate\Http\Request;
use App\popup;

class HomePageController extends Controller{

 public function index(){
  $sliders=Slider::all();
  $testimonials=Testimonial::all();
   $allnews=News::all();
   $allevents=Event::all();
   $modals=popup::Where('active', '=','1')
          ->get();
          if(sizeof($modals) == 0){
            // echo("<script>console.log('modal is null');</script>");
            $modals=popup::latest()
          ->get();
          }

  return view('index',compact('testimonials','sliders','allnews','allevents','modals'));
 }

public function about(){
 return view('about');
}
public function brochure(){
  return view('brochure');  
}

public function magazine(){
  return view('magazine');
}
public function starwell(){
 return view('starwellness');
}

public function contact(){
 return view('contact');
}

public function pcrTestRequest(){
 return view('pcrtestrequest');
}

public function boarddirector(){
 return view('bod');
}

public function location(){
	return view('getdirection');
}

public function feedback(){
  return view('feedback');
}
public function doctor(){
  //$doctors=Doctor::paginate(12);
  $doctors=Doctor::where('is_listed', '1')->orderBy('name', 'desc')->paginate(12);
  $departments=Department::all();
  return view('doctor',compact('doctors','departments'));
}
public function service($id){
  $allService=Service::all();
  $services=Service::findOrFail($id);
 return view('servicedetails',compact('allService','services','id'));
}

public function serviceAjax(Request $request){
  $id=$request->id;
  $services=Service::findOrFail($id);
 return view('descriptionservice',compact('services'));
}

public function event(){
  $allevents=Event::paginate(9);
  return view('eventdetails',compact('allevents'));
}

public function eventdetail($id){
  $events=Event::findOrFail($id);
  $allevents=Event::all();
  return view('descriptionEvent',compact('events','allevents','id'));
}

public function eventAjax(Request $request){
  $id=$request->id;
  $events=Event::findOrFail($id);
 return view('descriptionsevent',compact('events'));
}

public function search(Request $request){
  //echo "hello world";
  if(isset($request->dept_id) && isset($request->searchDr)){
       $doc = Doctor::where('name','LIKE','%'.$request->searchDr.'%')
     ->Where('Dept_id', '=', $request->dept_id)
     ->Where('is_listed', '=','1')
    ->get();
}
    elseif(isset($request->dept_id)){
    //  print_r($request->dept_id);
      $doc = Doctor::where('Dept_id', '=',$request->dept_id)
      ->Where('is_listed', '=','1')
      ->orderBy('order_id','asc')->get();
      
      
    }
  elseif(isset($request->searchDr)){
    $doc = Doctor::where('name','LIKE','%'.$request->searchDr.'%')
    ->Where('is_listed', '=','1')
    ->get();
   }
   else{
    $doc=Doctor::Where('is_listed', '=','1')->get();
   }
   $departments = Department::all();
   return view('Doctorsearch', compact('doc', 'departments'));
}

public function newsdetail($id){
  $news=News::findOrFail($id);
  $allnews=News::all();
  return view('newsdetails',compact('news','allnews','id'));
}
public function newsAjax(Request $request){
 $id=$request->id;
 $news=News::findOrFail($id);
 return view('descriptionNews',compact('news'));
}

public function news(){
  $allnews=News::paginate(9);
  return view('news',compact('allnews'));
}

public function chairmanmsg(){
	return view('chairmanmessage');
}

public function messagemd(){
	return view('messagefrommd');
}

public function centerexcellence(){
	return view('centerforexcellence');
}

public function missionandvision(){
	return view('Mission&Vision');
}

public function appointment(){
  $doctors=Doctor::all();
  $departments=Department::all();
  return view('appointment',compact('doctors','departments'));
}

public function internationalparty(){
  return view('internationalparty');
}

public function qrpay(){
  return view('Qrpay');
}

public function doctordetails($id){
  $doctors=Doctor::findOrFail($id);
  $departments=Department::all();
  return view('doctorsdetails',compact('doctors','departments'));
}

public function searchmethod(Request $request){
  $departments = Department::all();
   $query = $request->input('search');
   if($query){
    session()->put('searchvalue', $query);
   }
   $queries = Doctor::where('name', 'LIKE', '%' . $query . '%')
              ->Where('is_listed', '=','1')
              ->get();
  return view('search', compact('queries', 'query','departments'));
   }
public function searchonRefresh(){
  $departments = Department::all();
   $query = session()->get('searchvalue');
   $queries = Doctor::where('name', 'LIKE', '%' . $query . '%')
   ->Where('is_listed', '=','1')
   ->get();
  return view('search', compact('queries', 'query','departments'));
}

   public function gallery(){
    $gallery=Gallery::all();
    return view('gallery',compact('gallery'));
   }
   public function galleryDetails($id){
      $galleries = Gallery::findOrFail($id);
       $gallery = GalleryDetails::all();
      return view('galleryDetails',compact('galleries','gallery'));
   }
    public function testimonialview(){
    $testimonials=Testimonial::all();
    return view('testimonial',compact('testimonials'));
   }
   public function testimonialdetails($id){
     $testimonialview=Testimonial::findOrFail($id);
    $testimonials=Testimonial::all();
   return view('testimonialdetails',compact('testimonialview','testimonials'));
   }
   public function samplecollection(){
    return view('samplecollect');
   }
   
}