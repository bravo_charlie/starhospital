<?php

namespace App\Http\Controllers;

use App\popup;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Str;

class PopupController extends Controller
{


     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $modals = popup::all();
        return view ('dashboard.modal.index',compact('modals'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view ('dashboard.modal.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
         $modals = new popup();
        $request->validate([
            'modalname' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $modals->modalname = $request->modalname;
        if(file_exists($request->file('image'))){
            $image = "modal".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/modal');
            $request->file('image')->move($location, $image);
            $modals->image = $image;
        }
        else{
            $modals->image = 'default-thumbnail.png';
        }        
        $modals->save();
        return redirect('/home/modal');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function show(popup $popup)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function edit(popup $popup,$id)
    {
       $modals = popup::findOrFail($id);
        return view ('dashboard.modal.edit',compact('modals'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, popup $popup,$id)
    {
        

        if (str_contains($id, '-')){
            echo("<script>console.log('PHP: HELLO inside "  . $id. "');</script>");
            $modalID = Str::before($id, '-');   
            $flag = Str::after($id, '-') == '1' ? '0' : '1';
            DB::update('update popups set active = ? where id = ?',[$flag,$modalID]);
            return redirect('/home/modal');
        }
        echo("<script>console.log('PHP: HELLO outside "  . $id. "');</script>");

        $modals = popup::findOrFail($id);
        $request->validate([
            'modalname' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $modals->modalname = $request->modalname;
        if(file_exists($request->file('image'))){
            $image = "modal".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/modal');
            $request->file('image')->move($location, $image);
            $modals->image = $image;
        }
        else{
            $modals->image = $modals->image;
        }        
        $modals->save();
        return redirect('/home/modal');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\popup  $popup
     * @return \Illuminate\Http\Response
     */
    public function destroy(popup $popup,$id)
    {
        $modals = popup::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
