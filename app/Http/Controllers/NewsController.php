<?php

namespace App\Http\Controllers;

use App\News;
use Illuminate\Http\Request;

class NewsController extends Controller
{

     public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $news = News::all();
        return view ('dashboard.news.index',compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view ('dashboard.news.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $news = new News();
        $request->validate([
            'newstitle' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $news->newstitle = $request->newstitle;
        $news->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "news".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/news');
            $request->file('image')->move($location, $image);
            $news->image = $image;
        }
        else{
            $news->image = 'default-thumbnail.png';
        }        
        $news->save();
        return redirect('/home/news');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news,$id)
    {
        $news = News::findOrFail($id);
        return view ('dashboard.news.edit',compact('news'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news,$id)
    {
         $news = News::findOrFail($id);
        $request->validate([
            'newstitle' => 'required',
            'description' => 'required',
            'image' => 'image|mimes:jpg,png,jpeg|'
        ]);
        $news->newstitle = $request->newstitle;
        $news->description = $request->description;
        if(file_exists($request->file('image'))){
            $image = "news".time().'.'.$request->file('image')->getclientOriginalExtension();
            $location = public_path('uploads/news');
            $request->file('image')->move($location, $image);
            $news->image = $image;
        }
        else{
            $news->image = $news->image;
        }        
        $news->save();
        return redirect('/home/news');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news,$id)
    {
        $news = News::findOrFail($id) ->delete();
        return redirect()->back();
    }
}
