<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Pcrtest extends Model
{
    protected $fillable = [
       'name', 'email', 'phone','address','message','sex','age'
    ];
}
