<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BookAppointment extends Model
{
    protected $fillable = [
        'DoctorDepartmentname','Doctorname','appointmentdate','reason',
         'name', 'email', 'phone','address','age','nation','Gender',
    ];
}
