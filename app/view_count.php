<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class view_count extends Model
{
     protected $fillable = [
    	'ip', 'session_id', 'view_count'
    ];
}
