<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InternationalParty extends Model
{
     protected $fillable = [
        'fullname', 'email', 'phone','address','age','nation','Gender','passportnumber',
    ];
}
